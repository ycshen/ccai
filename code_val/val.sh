#!/bin/bash
dataset_name="heart"
path_name="/data/ztl/CCAI/result_log-out/"
v_log_name="log_v/"
resu_log_name="log_resu/"
scse_log_name="log_scse/"
u25_log_name="log_u25/"
ulight_log_name="log_ulight/"
ffc_log_name="log_ffc/"
u2d_log_name="log_u2d/"
attention_log_name="log_attention/"
scse2d_log_name="log_scse2d/"
cople_log_name="log_cople/"
nest_log_name="log_nest/"
ffc1_log_name="log_ffc_1/"
u2d1_log_name="log_u2d_1/"
attention1_log_name="log_attention_1/"
scse2d1_log_name="log_scse2d_1/"
cople1_log_name="log_cople_1/"
nest1_log_name="log_nest_1/"
ffc2_log_name="log_ffc_2/"
u2d2_log_name="log_u2d_2/"
attention2_log_name="log_attention_2/"
scse2d2_log_name="log_scse2d_2/"
cople2_log_name="log_cople_2/"
nest2_log_name="log_nest_2/"
nest3_log_name="log_nest_3/"
ffc02_log_name="log_ffc_02/"
ffc12_log_name="log_ffc_12/"
ffc22_log_name="log_ffc_22/"
nest4_log_name="log_nest_4/"
nest5_log_name="log_nest_5/"
stacking3_log_name="log_stacking_3/"
stacking4_log_name="log_stacking_4/"
stacking5_log_name="log_stacking_5/"
nest6_log_name="log_nest_6/"
nest7_log_name="log_nest_7/"
nest8_log_name="log_nest_8/"
cat1_log_name="log_cat_1/"
cat2_log_name="log_cat_2/"
cat3_log_name="log_cat_3/"
swin_log_name="log_swin/"
transunet_log_name="log_transunet/"
utnet_log_name="log_utnet/"
mtnet_log_name="log_mtnet/"
file_name="/val_out"
v_log_path=$path_name$v_log_name$dataset_name$file_name
resu_log_path=$path_name$resu_log_name$dataset_name$file_name
scse_log_path=$path_name$scse_log_name$dataset_name$file_name
u25_log_path=$path_name$u25_log_name$dataset_name$file_name
ulight_log_path=$path_name$ulight_log_name$dataset_name$file_name
ffc_log_path=$path_name$ffc_log_name$dataset_name$file_name
u2d_log_path=$path_name$u2d_log_name$dataset_name$file_name
attention_log_path=$path_name$attention_log_name$dataset_name$file_name
scse2d_log_path=$path_name$scse2d_log_name$dataset_name$file_name
cople_log_path=$path_name$cople_log_name$dataset_name$file_name
nest_log_path=$path_name$nest_log_name$dataset_name$file_name
ffc1_log_path=$path_name$ffc1_log_name$dataset_name$file_name
u2d1_log_path=$path_name$u2d1_log_name$dataset_name$file_name
attention1_log_path=$path_name$attention1_log_name$dataset_name$file_name
scse2d1_log_path=$path_name$scse2d1_log_name$dataset_name$file_name
cople1_log_path=$path_name$cople1_log_name$dataset_name$file_name
nest1_log_path=$path_name$nest1_log_name$dataset_name$file_name
ffc2_log_path=$path_name$ffc2_log_name$dataset_name$file_name
u2d2_log_path=$path_name$u2d2_log_name$dataset_name$file_name
attention2_log_path=$path_name$attention2_log_name$dataset_name$file_name
scse2d2_log_path=$path_name$scse2d2_log_name$dataset_name$file_name
cople2_log_path=$path_name$cople2_log_name$dataset_name$file_name
nest2_log_path=$path_name$nest2_log_name$dataset_name$file_name
nest3_log_path=$path_name$nest3_log_name$dataset_name$file_name
ffc02_log_path=$path_name$ffc02_log_name$dataset_name$file_name
ffc12_log_path=$path_name$ffc12_log_name$dataset_name$file_name
ffc22_log_path=$path_name$ffc22_log_name$dataset_name$file_name
nest4_log_path=$path_name$nest4_log_name$dataset_name$file_name
nest5_log_path=$path_name$nest5_log_name$dataset_name$file_name
stacking3_log_path=$path_name$stacking3_log_name$dataset_name$file_name
stacking4_log_path=$path_name$stacking4_log_name$dataset_name$file_name
stacking5_log_path=$path_name$stacking5_log_name$dataset_name$file_name
nest6_log_path=$path_name$nest6_log_name$dataset_name$file_name
nest7_log_path=$path_name$nest7_log_name$dataset_name$file_name
nest8_log_path=$path_name$nest8_log_name$dataset_name$file_name
cat1_log_path=$path_name$cat1_log_name$dataset_name$file_name
cat2_log_path=$path_name$cat2_log_name$dataset_name$file_name
cat3_log_path=$path_name$cat3_log_name$dataset_name$file_name
swin_log_path=$path_name$swin_log_name$dataset_name$file_name
transunet_log_path=$path_name$transunet_log_name$dataset_name$file_name
utnet_log_path=$path_name$utnet_log_name$dataset_name$file_name
mtnet_log_path=$path_name$mtnet_log_name$dataset_name$file_name
#nohup python -u val_v.py $dataset_name > $v_log_path 2>&1 &
#nohup python -u val_resu.py $dataset_name > $resu_log_path 2>&1 &
#nohup python -u val_scse.py $dataset_name > $scse_log_path 2>&1 &
#nohup python -u val_u25.py $dataset_name > $u25_log_path 2>&1 &
#nohup python -u val_ulight.py $dataset_name > $ulight_log_path 2>&1 &
#nohup python -u val_ffc.py $dataset_name > $ffc_log_path 2>&1 &
#nohup python -u val_u2d.py $dataset_name > $u2d_log_path 2>&1 &
#nohup python -u val_attention.py $dataset_name > $attention_log_path 2>&1 &
#nohup python -u val_scse2d.py $dataset_name > $scse2d_log_path 2>&1 &
#nohup python -u val_cople.py $dataset_name > $cople_log_path 2>&1 &
#nohup python -u val_nest.py $dataset_name > $nest_log_path 2>&1 &
#nohup python -u val_ffc_1.py $dataset_name > $ffc1_log_path 2>&1 &
#nohup python -u val_u2d_1.py $dataset_name > $u2d1_log_path 2>&1 &
#nohup python -u val_attention_1.py $dataset_name > $attention1_log_path 2>&1 &
#nohup python -u val_scse2d_1.py $dataset_name > $scse2d1_log_path 2>&1 &
#nohup python -u val_cople_1.py $dataset_name > $cople1_log_path 2>&1 &
#nohup python -u val_nest_1.py $dataset_name > $nest1_log_path 2>&1 &
#nohup python -u val_ffc_2.py $dataset_name > $ffc2_log_path 2>&1 &
#nohup python -u val_u2d_2.py $dataset_name > $u2d2_log_path 2>&1 &
#nohup python -u val_attention_2.py $dataset_name > $attention2_log_path 2>&1 &
#nohup python -u val_scse2d_2.py $dataset_name > $scse2d2_log_path 2>&1 &
#nohup python -u val_cople_2.py $dataset_name > $cople2_log_path 2>&1 &
#nohup python -u val_nest_2.py $dataset_name > $nest2_log_path 2>&1 &
#nohup python -u val_nest_3.py $dataset_name > $nest3_log_path 2>&1 &
#nohup python -u val_ffc_02.py $dataset_name > $ffc02_log_path 2>&1 &
#nohup python -u val_ffc_12.py $dataset_name > $ffc12_log_path 2>&1 &
#nohup python -u val_ffc_22.py $dataset_name > $ffc22_log_path 2>&1 &
#nohup python -u val_nest_4.py $dataset_name > $nest4_log_path 2>&1 &
#nohup python -u val_nest_5.py $dataset_name > $nest5_log_path 2>&1 &
#nohup python -u val_nest_6.py $dataset_name > $nest6_log_path 2>&1 &
#nohup python -u val_nest_7.py $dataset_name > $nest7_log_path 2>&1 &
#nohup python -u val_nest_8.py $dataset_name > $nest8_log_path 2>&1 &
#nohup python -u val_cat_1.py $dataset_name > $cat1_log_path 2>&1 &
#nohup python -u val_cat_2.py $dataset_name > $cat2_log_path 2>&1 &
#nohup python -u val_cat_3.py $dataset_name > $cat3_log_path 2>&1 &
#nohup python -u val_swin.py $dataset_name > $swin_log_path 2>&1 &
#nohup python -u val_transunet.py $dataset_name > $transunet_log_path 2>&1 &
#nohup python -u val_utnet.py $dataset_name > $utnet_log_path 2>&1 &
nohup python -u val_mtnet.py $dataset_name > $mtnet_log_path 2>&1 &

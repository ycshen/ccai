"""

测试脚本
"""
import sys
sys.path.append("..")
import os
import torch
from code_net.cople_net_1 import COPLENet
import code_utilities.parameter as para
from code_val.liver_test import calculate
from code_val.heart_test import test_all_case


dataset_name = sys.argv[1]

csv_path = '/data/ztl/CCAI/results_csv/csv_cople_1/'+dataset_name+'/result_cople_1.xlsx'
os.environ['CUDA_VISIBLE_DEVICES'] = '3'

# 定义网络并加载参数
model_pth = para.module_path_cople_1+dataset_name+'/net.pth'
params = {'in_chns': 1,
              'class_num': 1,
              'feature_chns': [16, 32, 64, 128, 256],
              'dropout': [0.0, 0.0, 0.2, 0.3, 0.4],
              'bilinear': False,
              'has_dropout': True,
          'dataset_name': dataset_name}

net = torch.nn.DataParallel(COPLENet(params)).cuda()
net.load_state_dict(torch.load(model_pth))
net.eval()

val_data_pth = para.txt_pth + dataset_name + '/val.txt'

if dataset_name == 'heart':
    with open(val_data_pth, 'r') as f:
        image_list = f.readlines()
    image_list = [item.replace('\n', '') for item in image_list]
    test_all_case(csv_path, dataset_name, net, image_list, num_classes=1,
                  patch_size=(112, 112, 80), stride_xy=18, stride_z=4, test_save_path=para.pred_path_cople_1 + dataset_name + '/')
else:
    calculate(val_data_pth, net, dataset_name, csv_path, test_save_path=para.pred_path_cople_1 + dataset_name)



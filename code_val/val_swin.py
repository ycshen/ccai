"""

测试脚本
"""
import sys
sys.path.append("..")
import os
import torch
from code_net.vision_transformer import SwinUnet
import code_utilities.parameter as para
from code_val.liver_test import calculate
from code_val.heart_test import test_all_case


dataset_name = sys.argv[1]

csv_path = '/data/ztl/CCAI/results_csv/csv_swin/'+dataset_name+'/result_swin.xlsx'
os.environ['CUDA_VISIBLE_DEVICES'] = '3'

# 定义网络并加载参数
model_pth = para.module_path_swin+dataset_name+'/net.pth'
params = {'in_chans': 1,
          'is_val': True,
              'img_size': 224,
              'patch_size': 4,
              'num_classes': 1,
              'embed_dim': 96,
              'depths': [2, 2, 2, 2],
              'num_heads': [1, 4, 4, 8, 16], #1231
              'window_size': 7,
              'qkv_bias': True,
              'qk_scale': None,
              'drop_rate': 0.,
              'attn_drop_rate': 0.,
              'drop_path_rate': 0.1,
              'mlp_ratio': 4.,
              'use_checkpoint': False,
              'ape': False,
              'patch_norm': True
              }

net = torch.nn.DataParallel(SwinUnet(params)).cuda()
net.load_state_dict(torch.load(model_pth))
net.eval()

val_data_pth = para.txt_pth + dataset_name + '/val.txt'

if dataset_name == 'heart':
    with open(val_data_pth, 'r') as f:
        image_list = f.readlines()
    image_list = [item.replace('\n', '') for item in image_list]
    test_all_case(csv_path, dataset_name, net, image_list, num_classes=1,
                  patch_size=(112, 112, 80), stride_xy=18, stride_z=4, test_save_path=para.pred_path_swin + dataset_name + '/')
else:
    calculate(val_data_pth, net, dataset_name, csv_path, test_save_path=para.pred_path_swin + dataset_name)



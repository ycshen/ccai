import sys
sys.path.append("..")

import os
import copy
import collections
from time import time
import torch
import numpy as np
import pandas as pd
import scipy.ndimage as ndimage
import SimpleITK as sitk
from medpy import metric
import code_utilities.parameter as para



def calculate(val_data_pth, net, dataset_name, csv_path, test_save_path):

    file_name = []  # 文件名称
    time_pre_case = []  # 单例数据消耗时间
    val_data_list = []

    # 定义评价指标
    liver_score = collections.OrderedDict()
    liver_score['dice'] = []
    liver_score['jacard'] = []
    liver_score['hd'] = []
    liver_score['assd'] = []

    fh = open(val_data_pth, 'r')
    for line in fh:
        line = line.strip('\n')
        line = line.rstrip()
        words = line.split()
        val_data_list.append(words[0].replace('pdata', 'odata'))

    for file_index, file in enumerate(val_data_list):

        start = time()

        file_name.append(file)

        # 将CT读入内存
        ct = sitk.ReadImage(file)
        ct_array = sitk.GetArrayFromImage(ct)

        origin_shape = ct_array.shape

        # 将灰度值在阈值之外的截断掉
        ct_array[ct_array > para.upper] = para.upper
        ct_array[ct_array < para.lower] = para.lower

        # min max 归一化
        ct_array = ct_array.astype(np.float32)
        ct_array = ct_array / 200

        # 对CT使用双三次算法进行插值，插值之后的array依然是int16
        ct_array = ndimage.zoom(ct_array, (1, para.down_scale, para.down_scale), order=3)

        # 对slice过少的数据使用padding
        too_small = False
        if ct_array.shape[0] < para.size:
            depth = ct_array.shape[0]
            temp = np.ones((para.size, int(512 * para.down_scale), int(512 * para.down_scale))) * para.lower
            temp[0: depth] = ct_array
            ct_array = temp
            too_small = True

        # 滑动窗口取样预测
        start_slice = 0
        end_slice = start_slice + para.size - 1
        count = np.zeros((ct_array.shape[0], 512, 512), dtype=np.int16)
        probability_map = np.zeros((ct_array.shape[0], 512, 512), dtype=np.float32)

        with torch.no_grad():
            while end_slice < ct_array.shape[0]:
                ct_tensor = torch.FloatTensor(ct_array[start_slice: end_slice + 1]).cuda()
                ct_tensor = ct_tensor.unsqueeze(dim=0).unsqueeze(dim=0)

                outputs = net(ct_tensor)

                count[start_slice: end_slice + 1] += 1
                probability_map[start_slice: end_slice + 1] += np.squeeze(outputs.cpu().detach().numpy())

                # 由于显存不足，这里直接保留ndarray数据，并在保存之后直接销毁计算图
                del outputs

                start_slice += para.stride
                end_slice = start_slice + para.size - 1

            if end_slice != ct_array.shape[0] - 1:
                end_slice = ct_array.shape[0] - 1
                start_slice = end_slice - para.size + 1

                ct_tensor = torch.FloatTensor(ct_array[start_slice: end_slice + 1]).cuda()
                ct_tensor = ct_tensor.unsqueeze(dim=0).unsqueeze(dim=0)
                outputs = net(ct_tensor)

                count[start_slice: end_slice + 1] += 1
                probability_map[start_slice: end_slice + 1] += np.squeeze(outputs.cpu().detach().numpy())

                del outputs

            pred_seg = np.zeros_like(probability_map)
            pred_seg[probability_map >= (para.threshold * count)] = 1

            if too_small:
                temp = np.zeros((depth, 512, 512), dtype=np.float32)
                temp += pred_seg[0: depth]
                pred_seg = temp

        # 将金标准读入内存
        seg = sitk.ReadImage(file.replace('volume', 'segmentation').replace('ct', 'seg').replace('nii', 'nii.gz'),
                             sitk.sitkUInt8)
        seg_array = sitk.GetArrayFromImage(seg)
        if dataset_name == 'liver':
            seg_array[seg_array > 0] = 1
        else:
            seg_array[seg_array < 2] = 0
            seg_array[seg_array > 0] = 1


        # 对肝脏进行最大连通域提取,移除细小区域,并进行内部的空洞填充
        pred_seg = pred_seg.astype(np.uint8)
        liver_seg = copy.deepcopy(pred_seg)


        # 将预测的结果保存为nii数据
        pred_seg = sitk.GetImageFromArray(liver_seg)

        pred_seg.SetDirection(ct.GetDirection())
        pred_seg.SetOrigin(ct.GetOrigin())
        pred_seg.SetSpacing(ct.GetSpacing())

        sitk.WriteImage(pred_seg,
                        os.path.join(test_save_path, file.split('/')[-1].replace('volume', 'pred')))

        # 计算分割评价指标
        print(seg_array.shape)
        print(liver_seg.shape)

        seg_array = seg_array.transpose(1, 2, 0)
        liver_seg = liver_seg.transpose(1, 2, 0)
        liver_seg = liver_seg

        dice = metric.binary.dc(liver_seg, seg_array)
        jc = metric.binary.jc(liver_seg, seg_array)
        hd = metric.binary.hd95(liver_seg, seg_array)
        asd = metric.binary.asd(liver_seg, seg_array)
        liver_score['dice'].append(dice)
        liver_score['jacard'].append(jc)
        liver_score['assd'].append(asd)
        liver_score['hd'].append(hd)

        speed = time() - start
        time_pre_case.append(speed)

        print(file_index, 'this case: {} use {:.3f} s'.format(file.split('/')[-1], speed))
        print('package dice: {: 3f}'.format(dice))
        print('-----------------------')

    # 将评价指标写入到exel中
    liver_data = pd.DataFrame(liver_score, index=file_name)
    liver_data['time'] = time_pre_case

    liver_statistics = pd.DataFrame(index=['mean', 'std', 'min', 'max'], columns=list(liver_data.columns))
    liver_statistics.loc['mean'] = liver_data.mean()
    liver_statistics.loc['std'] = liver_data.std()
    liver_statistics.loc['min'] = liver_data.min()
    liver_statistics.loc['max'] = liver_data.max()

    writer = pd.ExcelWriter(csv_path)
    liver_data.to_excel(writer, dataset_name)
    liver_statistics.to_excel(writer, dataset_name + '_statistics')
    writer.save()



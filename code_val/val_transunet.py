"""

测试脚本
"""
import sys
sys.path.append("..")
import os
import torch
import argparse
from code_net.vit_seg_modeling import VisionTransformer as ViT_seg
from code_net.vit_seg_modeling import CONFIGS as CONFIGS_ViT_seg
import code_utilities.parameter as para
from code_val.liver_test import calculate
from code_val.heart_test import test_all_case


dataset_name = sys.argv[1]

csv_path = '/data/ztl/CCAI/results_csv/csv_transunet/'+dataset_name+'/result_transunet.xlsx'
os.environ['CUDA_VISIBLE_DEVICES'] = '3'

# 定义网络并加载参数
model_pth = para.module_path_transunet+dataset_name+'/net.pth'
params = {'in_chns': 1,
              'class_num': 1,
              'vit_name': 'R50-ViT-B_16',
          'n_skip': 3,
          'img_size': 224,
          'vit_patches_size': 16}

config_vit = CONFIGS_ViT_seg[params['vit_name']]
config_vit.n_classes = params['class_num']
config_vit.n_skip = params['n_skip']
if params['vit_name'].find('R50') != -1:
    config_vit.patches.grid = (int(params['img_size'] / params['vit_patches_size']), int(params['img_size'] / params['vit_patches_size']))

net = ViT_seg(config_vit, img_size=params['img_size'], num_classes=config_vit.n_classes, in_chans=params['in_chns'], is_val=True)
net = torch.nn.DataParallel(net).cuda()
net.load_state_dict(torch.load(model_pth))
net.eval()

val_data_pth = para.txt_pth + dataset_name + '/val.txt'

if dataset_name == 'heart':
    with open(val_data_pth, 'r') as f:
        image_list = f.readlines()
    image_list = [item.replace('\n', '') for item in image_list]
    test_all_case(csv_path, dataset_name, net, image_list, num_classes=1,
                  patch_size=(112, 112, 80), stride_xy=18, stride_z=4, test_save_path=para.pred_path_transunet + dataset_name + '/')
else:
    calculate(val_data_pth, net, dataset_name, csv_path, test_save_path=para.pred_path_transunet + dataset_name)



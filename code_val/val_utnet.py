"""

测试脚本
"""
import sys
sys.path.append("..")
import os
import torch
from code_net.utnet import UTNet
import code_utilities.parameter as para
from code_val.liver_test import calculate
from code_val.heart_test import test_all_case


dataset_name = sys.argv[1]

csv_path = '/data/ztl/CCAI/results_csv/csv_utnet/'+dataset_name+'/result_utnet.xlsx'
os.environ['CUDA_VISIBLE_DEVICES'] = '5'

# 定义网络并加载参数
model_pth = para.module_path_utnet+dataset_name+'/net.pth'
params = {'in_chans': 1,
              'base_chan': 16,
              'num_class': 1,
              'reduce_size': 7,
            'block_list': '1234',
            'num_blocks': [1,1,1,1],
            'num_heads': [4,4,4,4],

              }

net = UTNet(in_chan=params['in_chans'], base_chan=params['base_chan'], num_classes=params['num_class'], reduce_size=params['reduce_size'], block_list=params['block_list'], num_blocks=params['num_blocks'], num_heads=params['num_heads'], projection='interp', attn_drop=0.1, proj_drop=0.1, rel_pos=True, aux_loss=False, maxpool=True)

net = torch.nn.DataParallel(net).cuda()
net.load_state_dict(torch.load(model_pth))
net.eval()

val_data_pth = para.txt_pth + dataset_name + '/val.txt'

if dataset_name == 'heart':
    with open(val_data_pth, 'r') as f:
        image_list = f.readlines()
    image_list = [item.replace('\n', '') for item in image_list]
    test_all_case(csv_path, dataset_name, net, image_list, num_classes=1,
                  patch_size=(112, 112, 80), stride_xy=18, stride_z=4, test_save_path=para.pred_path_utnet + dataset_name + '/')
else:
    calculate(val_data_pth, net, dataset_name, csv_path, test_save_path=para.pred_path_utnet + dataset_name)



"""

测试脚本
"""
import sys
sys.path.append("..")
import os
import torch
from code_net.CATNet_3 import UNet2D
import code_utilities.parameter as para
from code_val.liver_test import calculate
from code_val.heart_test import test_all_case


dataset_name = sys.argv[1]

csv_path = '/data/ztl/CCAI/results_csv/csv_cat_3/'+dataset_name+'/result_cat_3.xlsx'
os.environ['CUDA_VISIBLE_DEVICES'] = '2'

# 定义网络并加载参数
model_pth = para.module_path_cat_3+dataset_name+'/net.pth'
params = {'in_chns': 1,
          'is_training': False,
              'feature_chns': [16, 32, 64, 128, 256],
              'dropout': [0, 0, 0.3, 0.4, 0.5],
              'class_num': 1,
              'bilinear': True,
              'input_resolution': [112, 112],
              'depth': [2, 2, 2, 2],
              'num_heads': [1, 4, 4, 8, 16], #1231
              'window_size': 7,
              'qkv_bias': True,
              'qk_scale': None,
              'drop_rate': 0.,
              'attn_drop_rate': 0.,
              'drop_path_rate': 0.1,
              'mlp_ratio': 4.,
              'use_checkpoint': False
              }

net = torch.nn.DataParallel(UNet2D(params)).cuda()
net.load_state_dict(torch.load(model_pth))
net.eval()

val_data_pth = para.txt_pth + dataset_name + '/val.txt'

if dataset_name == 'heart':
    with open(val_data_pth, 'r') as f:
        image_list = f.readlines()
    image_list = [item.replace('\n', '') for item in image_list]
    test_all_case(csv_path, dataset_name, net, image_list, num_classes=1,
                  patch_size=(112, 112, 80), stride_xy=18, stride_z=4, test_save_path=para.pred_path_cat_3 + dataset_name + '/')
else:
    calculate(val_data_pth, net, dataset_name, csv_path, test_save_path=para.pred_path_cat_3 + dataset_name)



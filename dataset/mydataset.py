import os
import sys
sys.path.append(os.path.split(sys.path[0])[0])
import random
import numpy as np
import SimpleITK as sitk
import torch
from torch.utils.data import Dataset as dataset
import code_utilities.parameter as para
from dataset.read_data import read_list, read_data

class MyDataset(dataset):
    def __init__(self, pth_txt, dataset_name, transform=None):
        self.pth_txt = pth_txt
        self.dataset_name = dataset_name
        self. transform = transform
        # fh = open(self.pth_txt, 'r')
        # data_list = []
        # for line in fh:
        #     line = line.strip('\n')
        #     line = line.rstrip()
        #     words = line.split()
        #     data_list.append((words[0], words[1]))
        self.data_list = read_list(self.pth_txt, self.dataset_name)
        self.len = len(self.data_list)

    def __getitem__(self, index):

        # ct_path, seg_path = self.data_list[index]
        # # 将CT和金标准读入到内存中
        # ct = sitk.ReadImage(ct_path, sitk.sitkInt16)
        # seg = sitk.ReadImage(seg_path, sitk.sitkUInt8)
        # ct_array = sitk.GetArrayFromImage(ct)
        # seg_array = sitk.GetArrayFromImage(seg)
        # # min max 归一化
        # ct_array = ct_array.astype(np.float32)
        # ct_array = ct_array / 200
        # # 在slice平面内随机选取48张slice
        # start_slice = random.randint(0, ct_array.shape[0] - para.size)
        # end_slice = start_slice + para.size - 1
        # ct_array = ct_array[start_slice:end_slice + 1, :, :]
        # seg_array = seg_array[start_slice:end_slice + 1, :, :]
        # # 处理完毕，将array转换为tensor
        # ct_array = torch.FloatTensor(ct_array).unsqueeze(0)
        # seg_array = torch.FloatTensor(seg_array)
        ct_array, seg_array = read_data(index, self.data_list, self.dataset_name, self.transform)

        return ct_array, seg_array

    def __len__(self):

        return self.len



import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.checkpoint as checkpoint
from einops import rearrange
from timm.models.layers import DropPath, to_2tuple, to_3tuple, trunc_normal_


class ConvTransformer(nn.Module):
    def __init__(self, in_channels=1, in_resolution=(112, 112, 80), num_class=1, base_channels=8, position_dropout_rate=0.1, num_layers=2, num_heads=8, hidden_dim=4096, attention_dropout_rate=0.1):
        '''
        embed_dim:
        in_channels (int): channels of input image (1)
        in_resolution (tuple(int, int)): (H, W, D) of input image (112,112, 80)
        base_channels: the first convolution layer will expand/shrink channels to base channel, each operation
            later are times of base_channels
        position_dropout_rate:
        num_layers: num of transformer layers
        hidden_dim:
        attention_dropout_rate:
        '''
        super().__init__()
        self.embed_dim = 8*base_channels

        self.unet_encoder = Unet(in_channels, base_channels=base_channels)
        self.bn = nn.BatchNorm3d(base_channels*8, affine=True)
        self.conv = nn.Conv3d(
            in_channels=base_channels*8,
            out_channels=self.embed_dim,
            kernel_size=3,
            stride=1,
            padding=1
        )  # doesn't change dim
        self.position_encoder = FixedPositionalEncoding(
            self.embed_dim,
        )
        self.position_dropout = nn.Dropout(p=position_dropout_rate)
        # TODO: change to Swin Transformer
        self.transformer = TransformerModel(
            self.embed_dim,
            num_layers,
            num_heads,
            hidden_dim,
            position_dropout_rate,
            attention_dropout_rate,
        )
        self.pre_head_ln = nn.LayerNorm(self.embed_dim)

        self.decoder1 = SwinDecoder(
            in_channels=8 * base_channels, 
            in_resolution=(in_resolution[0]//8, in_resolution[1]//8, in_resolution[2]//8),
            upsample=True,
            skip_channel=8 * base_channels,
            window_size=2)
        self.decoder2 = SwinDecoder(
            in_channels=4*base_channels,
            in_resolution=(in_resolution[0]//4, in_resolution[1]//4, in_resolution[2]//4),
            skip_channel=4*base_channels, 
            upsample=True,
            window_size=4,
        )
        self.decoder3 = SwinDecoder(
            in_channels=2*base_channels,
            in_resolution=(in_resolution[0]//2, in_resolution[1]//2, in_resolution[2]//2),
            skip_channel=2*base_channels,
            upsample=True,
            window_size=8
        )
        self.segmentation_head = nn.Conv3d(
            in_channels=base_channels*2, 
            out_channels=num_class, 
            kernel_size=3, 
            padding=1
        )

    def encode(self, x):
        # Apply UNnet on input (B, C, H, W, D)
        # x1: B, base_channels, H, W, D
        # x2: B, base_channels*2, H/2, W/2, D/2
        # x3: B, base_channels*4, H/4, W/4, D/4
        # x4: B, base_channels*8, H/8, W/8, D/8
        B, C, H, W, D = x.shape
        x1, x2, x3, x4 = self.unet_encoder(x)
        x = self.bn(x4)
        x = F.relu(x)                   # B, base_channels*8, H/8, W/8, D/8
        x = self.conv(x)                # B, base_channels*8, H/8, W/8, D/8
        x = x.permute(0, 2, 3, 4, 1)    # B, H/8, W/8, D/8, base_channels*8
        # B, H*W*D/512, base_channels*8
        x = x.view(x.size(0), -1, self.embed_dim)
        x = self.position_encoder(x)
        x = self.position_dropout(x)    # B, H*W*D/512, base_channels*8
        # apply transformer
        x = self.transformer(x)         # B, H*W*D/512, base_channels*8
        x = self.pre_head_ln(x)         # B, H*W*D/512, base_channels*8
        x = x.view(B, H//8, W//8, D//8, self.embed_dim)
        x = x.permute(0, 4, 1, 2, 3)    # convert to BCHWD format
        return x1, x2, x3, x4, x

    def decode(self, x, x1, x2, x3, x4):
        x = self.decoder1(x, skip_feature=x4)   # B, base_channels*4, H/4, W/4, D/4
        x = self.decoder2(x, skip_feature=x3)   # B, base_channels*2, H/2, W/2, D/2
        x = self.decoder3(x, skip_feature=x2)   # B, base_channels, H, W, D
        x = torch.concat((x, x1), dim=1)
        x = self.segmentation_head(x)
        return x

    def forward(self, x):
        x1, x2, x3, x4, x = self.encode(x)
        x = self.decode(x, x1, x2, x3, x4)
        return x


class SwinDecoder(nn.Module):
    def __init__(self, in_channels, in_resolution, upsample=None, skip_channel=0, window_size=4):
        super().__init__()
        '''
        SwinDecoder

        Expected input shape: (B, C, H, W, D)
        Expected output shape with same-shaped skip: (B, C/2, H*2, W*2, D*2)

        Args:
        in_resolution (tuple(int)): (H, W, D)
        window_size: local window size for swin transformer
        '''
        # brutally reduce two layers into one
        self.input_channel = in_channels
        self.input_resolution = in_resolution
        # self.output_depth = out_depth
        self.skip_channel = skip_channel
        # print('linear in: ', (in_channels+skip_channel)
        #       * np.prod(in_resolution))
        # print('inchannel', in_channels)
        # print('skip channel', skip_channel)
        # print('in_resolution', in_resolution)

        # TODO: Check
        self.layer_up = BasicLayerUp3D(
            dim=in_channels if skip_channel == 0 else int(skip_channel+skip_channel),
            input_resolution=in_resolution,
            num_heads=8,
            num_blocks=2,
            qkv_bias=True,
            window_size=window_size,
            upsample=upsample
        )

    def forward(self, x, skip_feature=None):
        '''
        x: 5d vector of (B, C, H, W, D)
        '''
        input_shape = x.shape
        B, C, H, W, D = input_shape
        assert H * W * \
            D == np.prod(
                self.input_resolution), 'height and width should match'
        assert C == self.input_channel, f"channel should match, expect {self.input_channel}, got {C}"
        if skip_feature is not None:
            # print('x shape:', input_shape)
            skip_shape = skip_feature.shape  # B C H W D
            # print('skip shape', skip_shape)
            x = torch.cat([x, skip_feature], dim=1) # B, C1+C2, H, W, D
            # print('concatenated shape:', x.shape)
            # x = self.concat_layer(x.view(B, -1))
            # x = x.view(skip_shape)

        x = self.layer_up(x) # (B (C1+C2)/4, H*2, W*2, D*2) if upsample else same
        return x


###############################################################################
#### UNet Start ###############################################################
class Unet(nn.Module):
    def __init__(self, in_channels=4, base_channels=16):
        super(Unet, self).__init__()

        self.InitConv = InitConv(
            in_channels=in_channels, out_channels=base_channels, dropout=0.2)
        self.EnBlock1 = EnBlock(in_channels=base_channels)
        self.EnDown1 = EnDown(in_channels=base_channels,
                              out_channels=base_channels*2)

        self.EnBlock2_1 = EnBlock(in_channels=base_channels*2)
        self.EnBlock2_2 = EnBlock(in_channels=base_channels*2)
        self.EnDown2 = EnDown(in_channels=base_channels*2,
                              out_channels=base_channels*4)

        self.EnBlock3_1 = EnBlock(in_channels=base_channels * 4)
        self.EnBlock3_2 = EnBlock(in_channels=base_channels * 4)
        self.EnDown3 = EnDown(in_channels=base_channels*4,
                              out_channels=base_channels*8)

        self.EnBlock4_1 = EnBlock(in_channels=base_channels * 8)
        self.EnBlock4_2 = EnBlock(in_channels=base_channels * 8)
        self.EnBlock4_3 = EnBlock(in_channels=base_channels * 8)
        self.EnBlock4_4 = EnBlock(in_channels=base_channels * 8)

    def forward(self, x):
        x = self.InitConv(x)

        x1_1 = self.EnBlock1(x)         # (1, base_channels, H, W, D)
        # print('x1_1 shape', x1_1.shape)
        x1_2 = self.EnDown1(x1_1)

        x2_1 = self.EnBlock2_1(x1_2)
        x2_1 = self.EnBlock2_2(x2_1)    # (1, base_channels*2, H/2, W/2, D/2)
        # print('x2_1.shape', x2_1.shape)
        x2_2 = self.EnDown2(x2_1)

        x3_1 = self.EnBlock3_1(x2_2)
        x3_1 = self.EnBlock3_2(x3_1)    # (1, base_channels*4, H/4, W/4, D/4)
        # print('x3_1.shape', x3_1.shape)
        x3_2 = self.EnDown3(x3_1)

        x4_1 = self.EnBlock4_1(x3_2)    # (1, base_channels*8, H/8, W/8, D/8)
        # print('x4_1.shape', x4_1.shape)
        x4_2 = self.EnBlock4_2(x4_1)
        x4_3 = self.EnBlock4_3(x4_2)
        output = self.EnBlock4_4(x4_3)  # (1, base_channels*8, H/8, W/8, D/8)

        return x1_1, x2_1, x3_1, output


def normalization(planes, norm='gn'):
    if norm == 'bn':
        m = nn.BatchNorm3d(planes)
    elif norm == 'gn':
        m = nn.GroupNorm(8, planes)
    elif norm == 'in':
        m = nn.InstanceNorm3d(planes)
    else:
        raise ValueError('normalization type {} is not supported'.format(norm))
    return m


class InitConv(nn.Module):
    def __init__(self, in_channels=4, out_channels=16, dropout=0.2):
        super(InitConv, self).__init__()

        self.conv = nn.Conv3d(in_channels, out_channels,
                              kernel_size=3, padding=1)
        self.dropout = dropout

    def forward(self, x):
        y = self.conv(x)
        y = F.dropout3d(y, self.dropout)

        return y


class EnBlock(nn.Module):
    def __init__(self, in_channels, norm='gn'):
        super(EnBlock, self).__init__()

        self.bn1 = normalization(in_channels, norm=norm)
        self.relu1 = nn.ReLU(inplace=True)
        self.conv1 = nn.Conv3d(in_channels, in_channels,
                               kernel_size=3, padding=1)

        self.bn2 = normalization(in_channels, norm=norm)
        self.relu2 = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv3d(in_channels, in_channels,
                               kernel_size=3, padding=1)

    def forward(self, x):
        x1 = self.bn1(x)
        x1 = self.relu1(x1)
        x1 = self.conv1(x1)
        y = self.bn2(x1)
        y = self.relu2(y)
        y = self.conv2(y)
        y = y + x

        return y


class EnDown(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(EnDown, self).__init__()
        self.conv = nn.Conv3d(in_channels, out_channels,
                              kernel_size=3, stride=2, padding=1)

    def forward(self, x):
        y = self.conv(x)

        return y

#### UNet End #################################################################
###############################################################################


class FixedPositionalEncoding(nn.Module):
    def __init__(self, embedding_dim, max_length=512):
        super(FixedPositionalEncoding, self).__init__()

        pe = torch.zeros(max_length, embedding_dim)
        position = torch.arange(0, max_length, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(
            torch.arange(0, embedding_dim, 2).float()
            * (-torch.log(torch.tensor(10000.0)) / embedding_dim)
        )
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[: x.size(0), :]
        return x


###############################################################################
#### Transformer start ########################################################

class SelfAttention(nn.Module):
    def __init__(
        self, dim, heads=8, qkv_bias=False, qk_scale=None, dropout_rate=0.0
    ):
        super().__init__()
        self.num_heads = heads
        head_dim = dim // heads
        self.scale = qk_scale or head_dim ** -0.5

        self.qkv = nn.Linear(dim, dim * 3, bias=qkv_bias)
        self.attn_drop = nn.Dropout(dropout_rate)
        self.proj = nn.Linear(dim, dim)
        self.proj_drop = nn.Dropout(dropout_rate)

    def forward(self, x):
        B, N, C = x.shape
        qkv = (
            self.qkv(x)
            .reshape(B, N, 3, self.num_heads, C // self.num_heads)
            .permute(2, 0, 3, 1, 4)
        )
        q, k, v = (
            qkv[0],
            qkv[1],
            qkv[2],
        )  # make torchscript happy (cannot use tensor as tuple)

        attn = (q @ k.transpose(-2, -1)) * self.scale
        attn = attn.softmax(dim=-1)
        attn = self.attn_drop(attn)

        x = (attn @ v).transpose(1, 2).reshape(B, N, C)
        x = self.proj(x)
        x = self.proj_drop(x)
        return x


class Residual(nn.Module):
    def __init__(self, fn):
        super().__init__()
        self.fn = fn

    def forward(self, x):
        return self.fn(x) + x


class PreNorm(nn.Module):
    def __init__(self, dim, fn):
        super().__init__()
        self.norm = nn.LayerNorm(dim)
        self.fn = fn

    def forward(self, x):
        return self.fn(self.norm(x))


class PreNormDrop(nn.Module):
    def __init__(self, dim, dropout_rate, fn):
        super().__init__()
        self.norm = nn.LayerNorm(dim)
        self.dropout = nn.Dropout(p=dropout_rate)
        self.fn = fn

    def forward(self, x):
        return self.dropout(self.fn(self.norm(x)))


class FeedForward(nn.Module):
    def __init__(self, dim, hidden_dim, dropout_rate):
        super().__init__()
        self.net = nn.Sequential(
            nn.Linear(dim, hidden_dim),
            nn.GELU(),
            nn.Dropout(p=dropout_rate),
            nn.Linear(hidden_dim, dim),
            nn.Dropout(p=dropout_rate),
        )

    def forward(self, x):
        return self.net(x)


class TransformerModel(nn.Module):
    def __init__(
        self,
        dim,
        depth,
        heads,
        mlp_dim,
        dropout_rate=0.1,
        attn_dropout_rate=0.1,
    ):
        super().__init__()
        layers = []
        for _ in range(depth):
            layers.extend(
                [
                    Residual(
                        PreNormDrop(
                            dim,
                            dropout_rate,
                            SelfAttention(dim, heads=heads,
                                          dropout_rate=attn_dropout_rate),
                        )
                    ),
                    Residual(
                        PreNorm(dim, FeedForward(dim, mlp_dim, dropout_rate))
                    ),
                ]
            )
        self.net = nn.Sequential(*layers)

    def forward(self, x):
        return self.net(x)


class IntermediateSequential(nn.Sequential):
    def __init__(self, *args, return_intermediate=True):
        super().__init__(*args)
        self.return_intermediate = return_intermediate

    def forward(self, input):
        if not self.return_intermediate:
            return super().forward(input)

        intermediate_outputs = {}
        output = input
        for name, module in self.named_children():
            output = intermediate_outputs[name] = module(output)

        return output, intermediate_outputs
#### Transformer End ##########################################################
###############################################################################


###############################################################################
#### SwinUnet, SwinTransformer ################################################

class BasicLayerUp3D(nn.Module):

    def __init__(self, dim, input_resolution, num_blocks, num_heads, window_size,
                 mlp_ratio=4., qkv_bias=True, qk_scale=None, drop=0., attn_drop=0.,
                 drop_path=0., norm_layer=nn.LayerNorm, upsample=None, use_checkpoint=False):
        """ A basic Swin Transformer layer for one stage.

        Expected input shape: (B, C, H, W, D)
        Return shape: same if not upsampled, else (B, C/4, H*2, W*2, D*2)


        Args:
            dim (int): Number of input channels.
            input_resolution (tuple[int]): (H, W, D) Input resolution.
            depth (int): Number of blocks.
            num_heads (int): Number of attention heads.
            window_size (int): Local window size.
            mlp_ratio (float): Ratio of mlp hidden dim to embedding dim.
            qkv_bias (bool, optional): If True, add a learnable bias to query, key, value. Default: True
            qk_scale (float | None, optional): Override default qk scale of head_dim ** -0.5 if set.
            drop (float, optional): Dropout rate. Default: 0.0
            attn_drop (float, optional): Attention dropout rate. Default: 0.0
            drop_path (float | tuple[float], optional): Stochastic depth rate. Default: 0.0
            norm_layer (nn.Module, optional): Normalization layer. Default: nn.LayerNorm
            downsample (nn.Module | None, optional): Downsample layer at the end of the layer. Default: None
            use_checkpoint (bool): Whether to use checkpointing to save memory. Default: False.
        """

        super().__init__()
        self.dim = dim
        self.input_resolution = input_resolution
        self.depth = num_blocks
        self.use_checkpoint = use_checkpoint

        # build blocks
        self.blocks = nn.ModuleList([
            SwinTransformerBlock3D(dim=dim, input_resolution=input_resolution,
                                 num_heads=num_heads, window_size=window_size,
                                 shift_size=0 if (
                                     i % 2 == 0) else window_size // 2,
                                 mlp_ratio=mlp_ratio,
                                 qkv_bias=qkv_bias, qk_scale=qk_scale,
                                 drop=drop, attn_drop=attn_drop,
                                 drop_path=drop_path[i] if isinstance(
                                     drop_path, list) else drop_path,
                                 norm_layer=norm_layer)
            for i in range(num_blocks)])

        # patch merging layer
        if upsample is not None:
            self.upsample = PatchExpand3D(
                input_resolution, dim=dim, dim_scale=2, norm_layer=norm_layer)
        else:
            self.upsample = None

    def forward(self, x):
        '''
        Expected input shape: (B, C, H, W, D)
        Return shape: same if not upsampled, else (B, C/4, H*2, W*2, D*2)
        '''
        for blk in self.blocks:
            if self.use_checkpoint:
                x = checkpoint.checkpoint(blk, x)
            else:
                x = blk(x)
        if self.upsample is not None:
            x = self.upsample(x)
        return x


class SwinTransformerBlock3D(nn.Module):

    def __init__(self, dim, input_resolution, num_heads, window_size=7, shift_size=0,
                 mlp_ratio=4., qkv_bias=True, qk_scale=None, drop=0., attn_drop=0., drop_path=0.,
                 act_layer=nn.GELU, norm_layer=nn.LayerNorm):
        """ Swin Transformer Block.

        Expect input: B C H W D
        Output: B C H W D

        Args:
            dim (int): Number of input channels.
            input_resolution (tuple[int]): (H,W,D) Input resulotion.
            num_heads (int): Number of attention heads.
            window_size (int): Window size.
            shift_size (int): Shift size for SW-MSA.
            mlp_ratio (float): Ratio of mlp hidden dim to embedding dim.
            qkv_bias (bool, optional): If True, add a learnable bias to query, key, value. Default: True
            qk_scale (float | None, optional): Override default qk scale of head_dim ** -0.5 if set.
            drop (float, optional): Dropout rate. Default: 0.0
            attn_drop (float, optional): Attention dropout rate. Default: 0.0
            drop_path (float, optional): Stochastic depth rate. Default: 0.0
            act_layer (nn.Module, optional): Activation layer. Default: nn.GELU
            norm_layer (nn.Module, optional): Normalization layer.  Default: nn.LayerNorm
        """
        super().__init__()
        self.dim = dim
        self.input_resolution = input_resolution
        self.num_heads = num_heads
        self.window_size = window_size
        self.shift_size = shift_size
        self.mlp_ratio = mlp_ratio
        if min(self.input_resolution) <= self.window_size:
            # if window size is larger than input resolution, we don't partition windows
            self.shift_size = 0
            self.window_size = min(self.input_resolution)
        assert 0 <= self.shift_size < self.window_size, f"shift_size({self.shift_size}) must in 0-window_size({self.window_size})"

        self.norm1 = norm_layer(dim)
        # TODO: refine window size to let it changes with input image size
        self.attn = WindowAttention3D(
            dim, window_sizes=to_3tuple(self.window_size), num_heads=num_heads,
            qkv_bias=qkv_bias, qk_scale=qk_scale, attn_drop=attn_drop, proj_drop=drop)

        self.drop_path = DropPath(
            drop_path) if drop_path > 0. else nn.Identity()
        self.norm2 = norm_layer(dim)
        mlp_hidden_dim = int(dim * mlp_ratio)
        self.mlp = Mlp(in_features=dim, hidden_features=mlp_hidden_dim,
                       act_layer=act_layer, drop=drop)

        if self.shift_size > 0:
            # calculate attention mask for SW-MSA
            H, W, D = self.input_resolution
            img_mask = torch.zeros((1, H, W, D, 1))  # 1 H W D 1
            h_slices = (slice(0, -self.window_size),
                        slice(-self.window_size, -self.shift_size),
                        slice(-self.shift_size, None))
            w_slices = (slice(0, -self.window_size),
                        slice(-self.window_size, -self.shift_size),
                        slice(-self.shift_size, None))
            d_slices = (slice(0, -self.window_size),
                        slice(-self.window_size, -self.shift_size),
                        slice(-self.shift_size, None))
            cnt = 0
            for h in h_slices:
                for w in w_slices:
                    for d in d_slices:
                        img_mask[:, h, w, d, :] = cnt
                        cnt += 1

            # nW, window_size, window_size, window_size, 1
            mask_windows = window_partition_3d(img_mask, self.window_size)
            mask_windows = mask_windows.view(-1,
                                             self.window_size * self.window_size * self.window_size)
            attn_mask = mask_windows.unsqueeze(1) - mask_windows.unsqueeze(2)
            attn_mask = attn_mask.masked_fill(
                attn_mask != 0, float(-100.0)).masked_fill(attn_mask == 0, float(0.0))
        else:
            attn_mask = None

        self.register_buffer("attn_mask", attn_mask)

    def forward(self, x):
        H, W, D = self.input_resolution
        _, C, H1, W1, D1 = x.shape
        assert H == H1 and W==W1 and D==D1, f"input dimention {H1},{W1},{D1} is not as expected {H},{W},{D}"

        x = x.permute(0, 2, 3, 4, 1) # convert BCHWD to BHWDC for layernorm
        shortcut = x
        x = self.norm1(x)
        # x = x.view((B, H, W, D, C))

        # cyclic shift
        if self.shift_size > 0:
            shifted_x = torch.roll(
                x,
                shifts=(-self.shift_size, -self.shift_size, -self.shift_size),
                dims=(1, 2, 3)  # B H W D C
            )
        else:
            shifted_x = x

        # partition windows
        # nW*B, window_size, window_size, window_size, C
        x_windows = window_partition_3d(shifted_x, self.window_size)
        # nW*B, window_size*window_size*window_size, C
        x_windows = x_windows.view(-1, self.window_size *
                                   self.window_size * self.window_size, C)

        # W-MSA/SW-MSA
        # nW*B, window_size*window_size*window_size, C
        attn_windows = self.attn(x_windows, mask=self.attn_mask)

        # merge windows
        attn_windows = attn_windows.view(-1, self.window_size,
                                         self.window_size, self.window_size, C)
        shifted_x = window_reverse_3d(
            attn_windows, self.window_size, H, W, D)  # B H' W' D' C

        # reverse cyclic shift
        if self.shift_size > 0:
            x = torch.roll(shifted_x, shifts=(
                self.shift_size, self.shift_size, self.shift_size), dims=(1, 2, 3))
        else:
            x = shifted_x

        # FFN
        x = shortcut + self.drop_path(x)
        x = x + self.drop_path(self.mlp(self.norm2(x)))
        x = x.permute(0, 4, 1, 2, 3)   # Convert BHWDC back to BCHWD

        return x

    def extra_repr(self) -> str:
        return f"dim={self.dim}, input_resolution={self.input_resolution}, num_heads={self.num_heads}, " \
               f"window_size={self.window_size}, shift_size={self.shift_size}, mlp_ratio={self.mlp_ratio}"


class WindowAttention3D(nn.Module):
    """ Window based multi-head self attention (W-MSA) module with relative position bias.
    It supports both of shifted and non-shifted window.

    Args:
        dim (int): Number of input channels.
        window_size (tuple[int]): The height, width and depth of the window.
        num_heads (int): Number of attention heads.
        qkv_bias (bool, optional):  If True, add a learnable bias to query, key, value. Default: True
        qk_scale (float | None, optional): Override default qk scale of head_dim ** -0.5 if set
        attn_drop (float, optional): Dropout ratio of attention weight. Default: 0.0
        proj_drop (float, optional): Dropout ratio of output. Default: 0.0
    """

    def __init__(self, dim, window_sizes, num_heads, qkv_bias=True, qk_scale=None, attn_drop=0., proj_drop=0.):

        super().__init__()
        self.dim = dim
        self.window_sizes = window_sizes  # Wh, Ww, Wd
        self.num_heads = num_heads
        head_dim = dim // num_heads
        self.scale = qk_scale or head_dim ** -0.5

        # define a parameter table of relative position bias
        self.relative_position_bias_table = nn.Parameter(torch.zeros((2 * window_sizes[0] - 1) *
                                                                     (2 * window_sizes[1] - 1) * (2 * window_sizes[2] - 1), num_heads))  # 2*Wh-1 * 2*Ww-1 * 2*Wd-1, nH

        # get pair-wise relative position index for each token inside the window
        coords_h = torch.arange(self.window_sizes[0])
        coords_w = torch.arange(self.window_sizes[1])
        coords_d = torch.arange(self.window_sizes[2])
        coords = torch.stack(torch.meshgrid(
            [coords_h, coords_w, coords_d], indexing='ij'))  # (3, Wh, Ww, Wd), coordinate xyz of a pixel
        coords_flatten = torch.flatten(coords, 1)  # 3, Wh*Ww*Wd
        # The distance of every element to every element in a window in 3 dimensions (HWD)
        relative_coords = coords_flatten[:, :, None] - \
            coords_flatten[:, None, :]  # 3, Wh*Ww*Wd, Wh*Ww*Wd
        relative_coords = relative_coords.permute(
            1, 2, 0).contiguous()  # Wh*Ww*Wd, Wh*Ww*Wd, 2
        relative_coords[:, :, 0] += self.window_sizes[0] - \
            1  # shift to start from 0
        relative_coords[:, :, 1] += self.window_sizes[1] - 1
        relative_coords[:, :, 2] += self.window_sizes[2] - 1
        relative_coords[:, :, 0] *= (2 * self.window_sizes[1] - 1) * \
            (2 * self.window_sizes[2] - 1)
        relative_coords[:, :, 1] *= 2 * self.window_sizes[2] - 1
        relative_position_index = relative_coords.sum(-1)  # Wh*Ww, Wh*Ww
        self.register_buffer("relative_position_index",
                             relative_position_index)

        self.qkv = nn.Linear(dim, dim * 3, bias=qkv_bias)
        self.attn_drop = nn.Dropout(attn_drop)
        self.proj = nn.Linear(dim, dim)
        self.proj_drop = nn.Dropout(proj_drop)

        trunc_normal_(self.relative_position_bias_table, std=.02)
        self.softmax = nn.Softmax(dim=-1)

    def forward(self, x, mask=None):
        """
        Args:
            x: input features with shape of (num_windows*B, N, C)
            mask: (0/-inf) mask with shape of (num_windows, Wh*Ww, Wh*Ww) or None
        """
        B_, N, C = x.shape
        qkv = self.qkv(x).reshape(B_, N, 3, self.num_heads, C //
                                  self.num_heads).permute(2, 0, 3, 1, 4)
        # make torchscript happy (cannot use tensor as tuple)
        q, k, v = qkv[0], qkv[1], qkv[2]

        q = q * self.scale
        attn = (q @ k.transpose(-2, -1))

        relative_position_bias = self.relative_position_bias_table[self.relative_position_index.view(-1)].view(
            self.window_sizes[0] * self.window_sizes[1] * self.window_sizes[2], 
            self.window_sizes[0] * self.window_sizes[1] *  self.window_sizes[2], 
            -1)  # Wh*Ww*Wd,Wh*Ww*Wd,nH
        relative_position_bias = relative_position_bias.permute(
            2, 0, 1).contiguous()  # nH, Wh*Ww, Wh*Ww
        attn = attn + relative_position_bias.unsqueeze(0)

        if mask is not None:
            nW = mask.shape[0]
            attn = attn.view(B_ // nW, nW, self.num_heads, N,
                             N) + mask.unsqueeze(1).unsqueeze(0)
            attn = attn.view(-1, self.num_heads, N, N)
            attn = self.softmax(attn)
        else:
            attn = self.softmax(attn)

        attn = self.attn_drop(attn)

        x = (attn @ v).transpose(1, 2).reshape(B_, N, C)
        x = self.proj(x)
        x = self.proj_drop(x)
        return x

    def extra_repr(self) -> str:
        return f'dim={self.dim}, window_size={self.window_sizes}, num_heads={self.num_heads}'

    def flops(self, N):
        # calculate flops for 1 window with token length of N
        flops = 0
        # qkv = self.qkv(x)
        flops += N * self.dim * 3 * self.dim
        # attn = (q @ k.transpose(-2, -1))
        flops += self.num_heads * N * (self.dim // self.num_heads) * N
        #  x = (attn @ v)
        flops += self.num_heads * N * N * (self.dim // self.num_heads)
        # x = self.proj(x)
        flops += N * self.dim * self.dim
        return flops


class Mlp(nn.Module):
    def __init__(self, in_features, hidden_features=None, out_features=None, act_layer=nn.GELU, drop=0.):
        super().__init__()
        out_features = out_features or in_features
        hidden_features = hidden_features or in_features
        self.fc1 = nn.Linear(in_features, hidden_features)
        self.act = act_layer()
        self.fc2 = nn.Linear(hidden_features, out_features)
        self.drop = nn.Dropout(drop)

    def forward(self, x):
        x = self.fc1(x)
        x = self.act(x)
        x = self.drop(x)
        x = self.fc2(x)
        x = self.drop(x)
        return x


class PatchExpand3D(nn.Module):
    def __init__(self, input_resolution, dim, dim_scale=2, norm_layer=nn.LayerNorm):
        '''
        input_resolution (tuple(int)): a tuple of (H, W, D)
        dim (int): embed_dim 
        dim_scale: scaling dim by dim_scale
        '''
        super().__init__()
        self.input_resolution = input_resolution
        self.dim = dim
        self.dim_scale = dim_scale
        self.expand = nn.Linear(
            dim, 2*dim, bias=False) if dim_scale == 2 else nn.Identity()
        self.norm = norm_layer(dim * dim_scale // 8)

    def forward(self, x):
        """
        x: B, C(embed dim), H, W, D
        """
        H, W, D = self.input_resolution
        B, C, H1, W1, D1 = x.shape
        assert H1==H and W1==W and D1==D, f"Input shape {H1},{W1},{D1} not as expected ({H},{W},{D})"
        # print("before expand shape (BCHWD):", x.shape)
        # print("input_resolution (HWD)", self.input_resolution)
        x = x.permute(0, 2, 3, 4, 1)    # convert BCHWD to BHWDC
        x = self.expand(x)
        # x.permute(0, 4, 1, 2, 3)    
        # B, L, C = x.shape
        # assert L == H * W * D, "input feature has wrong size"
        # assert C == self.dim * self.dim_scale, f"expanded feature should have {self.dim_scale} times dim"

        # x = x.view(B, H, W, D, C)
        x = rearrange(x, 'b h w d (p1 p2 p3 c)-> b (h p1) (w p2) (d p3) c',
                      b=B, p1=2, p2=2, p3=2)
        # x = x.view(B, -1, C//8)
        x = self.norm(x)
        x = x.permute(0, 4, 1, 2, 3) # convert BHWDC to BCHWD
        return x

def window_partition_3d(x, window_size):
    """
    Args:
        x: (B, H, W, D, C)
        window_size (int): window size

    Returns:
        windows: (num_windows*B, window_size, window_size, window_size, C)
    """
    B, H, W, D, C = x.shape
    x = x.view(
        B,
        H // window_size, window_size,
        W // window_size, window_size,
        D // window_size, window_size,
        C
    )
    windows = x.permute(0, 1, 3, 5, 2, 4, 6, 7).contiguous(
    ).view(-1, window_size, window_size, window_size, C)
    return windows


def window_reverse_3d(windows, window_size, H, W, D):
    """
    Args:
        windows: (num_windows*B, window_size, window_size, window_size, C)
        window_size (int): Window size
        H (int): Height of image
        W (int): Width of image
        D (int): Depth of image
    Returns:
        x: (B, H, W, D, C)
    """
    B = int(windows.shape[0] / (H * W * D /
            window_size / window_size / window_size))
    x = windows.view(B, H // window_size, W // window_size, D // window_size,
                     window_size, window_size, window_size, -1)
    x = x.permute(0, 1, 4, 2, 5, 3, 6, 7).contiguous().view(B, H, W, D, -1)
    return x

###############################################################################
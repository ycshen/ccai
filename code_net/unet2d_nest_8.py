# -*- coding: utf-8 -*-
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from code_net.FFCNet_02 import *


class unetUp_origin(nn.Module):
    def __init__(self, in_size, out_size, n_concat=2):
        super(unetUp_origin, self).__init__()

        self.conv = ConvBlock(in_size + (n_concat - 2) * out_size, out_size)
        self.up = nn.ConvTranspose2d(in_size, out_size, kernel_size=2, stride=2)

    def forward(self, inputs0, *input):

        outputs0 = self.up(inputs0)

        for i in range(len(input)):
            outputs0 = torch.cat([outputs0, input[i]], 1)
        return self.conv(outputs0)

class unetUp_origin_1(nn.Module):
    def __init__(self, in_size, out_size, n_concat=2):
        super(unetUp_origin_1, self).__init__()

        self.conv = ConvBlock(in_size + (n_concat - 2) * out_size + int(in_size / 4), out_size)
        self.up = nn.ConvTranspose2d(in_size, out_size, kernel_size=2, stride=2)

    def forward(self, inputs0, *input):
        outputs0 = self.up(inputs0)
        for i in range(len(input)):
            outputs0 = torch.cat([outputs0, input[i]], 1)
        return self.conv(outputs0)


class NestedUNet2D(nn.Module):

    def __init__(self, params):
        super(NestedUNet2D, self).__init__()
        self.params = params
        self.in_chns = self.params['in_chns']
        self.filters = self.params['feature_chns']
        self.n_class = self.params['class_num']
        self.bilinear = self.params['bilinear']
        self.dropout = self.params['dropout']
        self.is_trainging = self.params['is_training']
        self.is_stacking = self.params['is_stacking']
        self.majority = self.params['majority']
        # filters = [int(x / self.feature_scale) for x in filters]

        # downsampling
        self.conv00 = ConvBlock(self.in_chns, self.filters[0], self.dropout[0])
        self.maxpool0 = nn.MaxPool2d(kernel_size=2)
        self.conv10 = ConvBlock(self.filters[0], self.filters[1], self.dropout[1])
        self.maxpool1 = nn.MaxPool2d(kernel_size=2)
        self.conv20 = ConvBlock(self.filters[1], self.filters[2], self.dropout[2])
        self.maxpool2 = nn.MaxPool2d(kernel_size=2)
        self.conv30 = ConvBlock(self.filters[2], self.filters[3], self.dropout[3])
        self.maxpool3 = nn.MaxPool2d(kernel_size=2)
        self.conv40 = ConvBlock(self.filters[3], self.filters[4], self.dropout[4])


        # upsampling
        self.up_concat01 = unetUp_origin(self.filters[1], self.filters[0])
        self.up_concat11 = unetUp_origin_1(self.filters[2], self.filters[1])
        self.maxpool11 = nn.MaxPool2d(kernel_size=2)
        self.up_concat21 = unetUp_origin_1(self.filters[3], self.filters[2])
        self.up_concat31 = unetUp_origin_1(self.filters[4], self.filters[3])

        self.up_concat02 = unetUp_origin(self.filters[1], self.filters[0], 3)
        self.up_concat12 = unetUp_origin(self.filters[2], self.filters[1], 3)
        self.up_concat22 = unetUp_origin_1(self.filters[3], self.filters[2], 3)

        self.up_concat03 = unetUp_origin(self.filters[1], self.filters[0], 4)
        self.up_concat13 = unetUp_origin(self.filters[2], self.filters[1], 4)

        self.up_concat04 = unetUp_origin(self.filters[1], self.filters[0], 5)

        # final conv (without any concat)
        self.final_1 = nn.Sequential(nn.Conv2d(self.filters[0], self.n_class,
                                                kernel_size=3, padding=1),
                                      nn.Sigmoid())
        self.final_2 = nn.Sequential(nn.Conv2d(self.filters[0], self.n_class,
                                                kernel_size=3, padding=1),
                                      nn.Sigmoid())
        self.final_3 = nn.Sequential(nn.Conv2d(self.filters[0], self.n_class,
                                                kernel_size=3, padding=1),
                                      nn.Sigmoid())
        self.final_4 = nn.Sequential(nn.Conv2d(self.filters[0], self.n_class,
                                                kernel_size=3, padding=1),
                                      nn.Sigmoid())


    def forward(self, inputs):
        x_shape = list(inputs.shape)
        if (len(x_shape) == 5):
            [N, C, H, W, D] = x_shape
            new_shape = [N * D, C, H, W]
            inputs = torch.transpose(inputs, 1, 4)  # [N, D, H, W, C]
            inputs = torch.transpose(inputs, 2, 4)  # [N, D, C, W, H]
            inputs = torch.transpose(inputs, 3, 4)  # [N, D, C, H, W]
            inputs = torch.reshape(inputs, new_shape)
        # column : 0
        X_00 = self.conv00(inputs)
        maxpool0 = self.maxpool0(X_00)
        X_10 = self.conv10(maxpool0)
        maxpool1 = self.maxpool1(X_10)
        X_20 = self.conv20(maxpool1)
        maxpool2 = self.maxpool2(X_20)
        X_30 = self.conv30(maxpool2)
        maxpool3 = self.maxpool3(X_30)
        X_40 = self.conv40(maxpool3)

        # column : 1
        X_01 = self.up_concat01(X_10, X_00)
        X_11 = self.up_concat11(X_20, X_10, maxpool0)
        maxpool11 = self.maxpool11(X_11)
        X_21 = self.up_concat21(X_30, X_20, maxpool1)
        X_31 = self.up_concat31(X_40, X_30, maxpool2)
        # column : 2
        X_02 = self.up_concat02(X_11, X_00, X_01)
        X_12 = self.up_concat12(X_21, X_10, X_11)
        X_22 = self.up_concat22(X_31, X_20, X_21, maxpool11)
        # column : 3
        X_03 = self.up_concat03(X_12, X_00, X_01, X_02)
        X_13 = self.up_concat13(X_22, X_10, X_11, X_12)
        # column : 4
        X_04 = self.up_concat04(X_13, X_00, X_01, X_02, X_03)

        # final layer
        final_1 = self.final_1(X_01)
        final_2 = self.final_2(X_02)
        final_3 = self.final_3(X_03)
        final_4 = self.final_4(X_04)

        if self.is_stacking:
            new_shape = [N, D] + list(final_4.shape)[1:]  # [N, D, C, H, W]

            final_4 = torch.reshape(final_4, new_shape)  # [N, D, C, H, W]
            final_4 = torch.transpose(final_4, 1, 2)  # [N, C, D, H, W]
            final_4 = torch.transpose(final_4, 2, 4)  # [N, C, W, H, D]
            final_4 = torch.transpose(final_4, 2, 3)  # [N, C, H, W, D]

            final_3 = torch.reshape(final_3, new_shape)  # [N, D, C, H, W]
            final_3 = torch.transpose(final_3, 1, 2)  # [N, C, D, H, W]
            final_3 = torch.transpose(final_3, 2, 4)  # [N, C, W, H, D]
            final_3 = torch.transpose(final_3, 2, 3)  # [N, C, H, W, D]

            final_2 = torch.reshape(final_2, new_shape)  # [N, D, C, H, W]
            final_2 = torch.transpose(final_2, 1, 2)  # [N, C, D, H, W]
            final_2 = torch.transpose(final_2, 2, 4)  # [N, C, W, H, D]
            final_2 = torch.transpose(final_2, 2, 3)  # [N, C, H, W, D]

            final_1 = torch.reshape(final_1, new_shape)  # [N, D, C, H, W]
            final_1 = torch.transpose(final_1, 1, 2)  # [N, C, D, H, W]
            final_1 = torch.transpose(final_1, 2, 4)  # [N, C, W, H, D]
            final_1 = torch.transpose(final_1, 2, 3)  # [N, C, H, W, D]
            final = torch.cat([final_1, final_2, final_3, final_4], 1)

            return final
        else:
            if self.is_trainging:
                final = (final_1 + final_2 + final_3 + final_4) / 4
            else:
                if self.majority:
                    final_cat = torch.cat([final_1, final_2, final_3, final_4], 1)
                    final_sorted, indices = torch.sort(final_cat, dim=1)
                    final = torch.unsqueeze(final_sorted[:, 2, :, :], 1)

                else:
                    final = final_1
            if (len(x_shape) == 5):
                new_shape = [N, D] + list(final.shape)[1:]  # [N, D, C, H, W]
                final = torch.reshape(final, new_shape)  # [N, D, C, H, W]
                final = torch.transpose(final, 1, 2)  # [N, C, D, H, W]
                final = torch.transpose(final, 2, 4)  # [N, C, W, H, D]
                final = torch.transpose(final, 2, 3)  # [N, C, H, W, D]
            return final








if __name__ == "__main__":
    params = {'in_chns': 1,
              'feature_chns': [16, 32, 64, 128, 256],
              'dropout': [0, 0, 0.3, 0.4, 0.5],
              'class_num': 1,
              'bilinear': False}
    Net = NestedUNet2D(params)
    Net = Net.double()

    x = np.random.rand(1, 1, 112, 112, 80)
    xt = torch.from_numpy(x)
    xt = torch.tensor(xt)

    y = Net(xt)
    print(len(y.size()))
    y = y.detach().numpy()
    print(y.shape)



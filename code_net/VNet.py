import torch
from torch import nn
import torch.nn.functional as F
import numpy as np

class ConvBlock(nn.Module):
    def __init__(self, n_stages, n_filters_in, n_filters_out, normalization='none'):
        super(ConvBlock, self).__init__()

        ops = []
        for i in range(n_stages):
            if i==0:
                input_channel = n_filters_in
            else:
                input_channel = n_filters_out

            ops.append(nn.Conv3d(input_channel, n_filters_out, 3, padding=1))
            if normalization == 'batchnorm':
                ops.append(nn.BatchNorm3d(n_filters_out))
            elif normalization == 'groupnorm':
                ops.append(nn.GroupNorm(num_groups=16, num_channels=n_filters_out))
            elif normalization == 'instancenorm':
                ops.append(nn.InstanceNorm3d(n_filters_out))
            # elif normalization != 'none':
            #     assert False
            ops.append(nn.LeakyReLU())

        self.conv = nn.Sequential(*ops)

    def forward(self, x):
        x = self.conv(x)
        return x


class ResidualConvBlock(nn.Module):
    def __init__(self, n_stages, n_filters_in, n_filters_out, dropout_p, normalization='none'):
        super(ResidualConvBlock, self).__init__()

        ops = []
        for i in range(n_stages):
            if i == 0:
                input_channel = n_filters_in
            else:
                input_channel = n_filters_out

            ops.append(nn.Conv3d(input_channel, n_filters_out, 3, padding=1))
            if normalization == 'batchnorm':
                ops.append(nn.BatchNorm3d(n_filters_out))
            elif normalization == 'groupnorm':
                ops.append(nn.GroupNorm(num_groups=16, num_channels=n_filters_out))
            elif normalization == 'instancenorm':
                ops.append(nn.InstanceNorm3d(n_filters_out))
            elif normalization != 'none':
                assert False

            if i != n_stages-1:
                ops.append(nn.LeakyReLU())
                ops.append(nn.Dropout(dropout_p))

        self.conv = nn.Sequential(*ops)
        self.relu = nn.LeakyReLU()

    def forward(self, x):
        x = (self.conv(x) + x)
        x = self.relu(x)
        return x


class DownsamplingConvBlock(nn.Module):
    def __init__(self, n_filters_in, n_filters_out, stride=2, normalization='none'):
        super(DownsamplingConvBlock, self).__init__()

        ops = []
        if normalization != 'none':
            ops.append(nn.Conv3d(n_filters_in, n_filters_out, stride, padding=0, stride=stride))
            if normalization == 'batchnorm':
                ops.append(nn.BatchNorm3d(n_filters_out))
            elif normalization == 'groupnorm':
                ops.append(nn.GroupNorm(num_groups=16, num_channels=n_filters_out))
            elif normalization == 'instancenorm':
                ops.append(nn.InstanceNorm3d(n_filters_out))
            # else:
            #     assert False
        else:
            ops.append(nn.Conv3d(n_filters_in, n_filters_out, stride, padding=0, stride=stride))

        ops.append(nn.LeakyReLU())

        self.conv = nn.Sequential(*ops)

    def forward(self, x):
        x = self.conv(x)
        return x


class UpsamplingDeconvBlock(nn.Module):
    def __init__(self, n_filters_in, n_filters_out, stride=2, normalization='none'):
        super(UpsamplingDeconvBlock, self).__init__()

        ops = []
        if normalization != 'none':
            ops.append(nn.ConvTranspose3d(n_filters_in, n_filters_out, stride, padding=0, stride=stride))
            if normalization == 'batchnorm':
                ops.append(nn.BatchNorm3d(n_filters_out))
            elif normalization == 'groupnorm':
                ops.append(nn.GroupNorm(num_groups=16, num_channels=n_filters_out))
            elif normalization == 'instancenorm':
                ops.append(nn.InstanceNorm3d(n_filters_out))
            # else:
            #     assert False
        else:
            ops.append(nn.ConvTranspose3d(n_filters_in, n_filters_out, stride, padding=0, stride=stride))

        ops.append(nn.LeakyReLU())

        self.conv = nn.Sequential(*ops)

    def forward(self, x):
        x = self.conv(x)
        return x


class Upsampling(nn.Module):
    def __init__(self, n_filters_in, n_filters_out, stride=2, normalization='none'):
        super(Upsampling, self).__init__()

        ops = []
        ops.append(nn.ConvTranspose3d(n_filters_in, n_filters_in, kernel_size=2, stride=2))
        ops.append(nn.Conv3d(n_filters_in, n_filters_out, kernel_size=3, padding=1))
        if normalization == 'batchnorm':
            ops.append(nn.BatchNorm3d(n_filters_out))
        elif normalization == 'groupnorm':
            ops.append(nn.GroupNorm(num_groups=16, num_channels=n_filters_out))
        elif normalization == 'instancenorm':
            ops.append(nn.InstanceNorm3d(n_filters_out))
        # elif normalization != 'none':
        #     assert False
        ops.append(nn.LeakyReLU())

        self.conv = nn.Sequential(*ops)

    def forward(self, x):
        x = self.conv(x)
        return x


class VNet(nn.Module):
    def __init__(self, params):
        super(VNet, self).__init__()
        self.params = params
        self.in_chns = self.params['in_chns']
        self.ft_chns = self.params['feature_chns']
        self.n_class = self.params['class_num']
        self.trilinear = self.params['trilinear']
        self.dropout = self.params['dropout']
        self.dataset_name = self.params['dataset_name']


        self.block_one = ResidualConvBlock(1, self.in_chns, self.ft_chns[0], self.dropout[0])
        self.block_one_dw = DownsamplingConvBlock(self.ft_chns[0], self.ft_chns[1], normalization='none')

        self.block_two = ResidualConvBlock(2, self.ft_chns[1], self.ft_chns[1], self.dropout[1])
        self.block_two_dw = DownsamplingConvBlock(self.ft_chns[1], self.ft_chns[2], normalization='none')

        self.block_three = ResidualConvBlock(3, self.ft_chns[2], self.ft_chns[2], self.dropout[2])
        self.block_three_dw = DownsamplingConvBlock(self.ft_chns[2], self.ft_chns[3], normalization='none')

        self.block_four = ResidualConvBlock(3, self.ft_chns[3], self.ft_chns[3], self.dropout[3])
        self.block_four_dw = DownsamplingConvBlock(self.ft_chns[3], self.ft_chns[4], normalization='none')

        self.block_five = ResidualConvBlock(3, self.ft_chns[4], self.ft_chns[4], self.dropout[4])
        self.block_five_up = UpsamplingDeconvBlock(self.ft_chns[4], self.ft_chns[3], normalization='none')

        self.block_six = ResidualConvBlock(3, self.ft_chns[3], self.ft_chns[3], 0)
        self.block_six_up = UpsamplingDeconvBlock(self.ft_chns[3], self.ft_chns[2], normalization='none')

        self.block_seven = ResidualConvBlock(3, self.ft_chns[2], self.ft_chns[2], 0)
        self.block_seven_up = UpsamplingDeconvBlock(self.ft_chns[2], self.ft_chns[1], normalization='none')

        self.block_eight = ResidualConvBlock(2, self.ft_chns[1], self.ft_chns[1], 0)
        self.block_eight_up = UpsamplingDeconvBlock(self.ft_chns[1], self.ft_chns[0], normalization='none')

        self.block_nine = ResidualConvBlock(1, self.ft_chns[0], self.ft_chns[0], 0)
        if self.dataset_name == 'heart':
            self.out_conv = nn.Sequential(
                nn.Conv3d(self.ft_chns[0], self.n_class, 1, padding=0),
                nn.Sigmoid()
            )
        else:
            self.out_conv = nn.Sequential(
                nn.Conv3d(self.ft_chns[0], int(self.ft_chns[0] / 2), kernel_size=3, padding=1),
                nn.LeakyReLU(),
                nn.ConvTranspose3d(int(self.ft_chns[0] / 2), self.n_class, (1, 2, 2), (1, 2, 2)),
                nn.Sigmoid()
            )

        # self.__init_weight()

    def encoder(self, input):
        x1 = self.block_one(input)
        x1_dw = self.block_one_dw(x1)

        x2 = self.block_two(x1_dw)
        x2_dw = self.block_two_dw(x2)

        x3 = self.block_three(x2_dw)
        x3_dw = self.block_three_dw(x3)

        x4 = self.block_four(x3_dw)
        x4_dw = self.block_four_dw(x4)

        x5 = self.block_five(x4_dw)

        res = [x1, x2, x3, x4, x5]

        return res

    def decoder(self, features):
        x1 = features[0]
        x2 = features[1]
        x3 = features[2]
        x4 = features[3]
        x5 = features[4]

        x5_up = self.block_five_up(x5)
        x5_up = x5_up + x4

        x6 = self.block_six(x5_up)
        x6_up = self.block_six_up(x6)
        x6_up = x6_up + x3

        x7 = self.block_seven(x6_up)
        x7_up = self.block_seven_up(x7)
        x7_up = x7_up + x2

        x8 = self.block_eight(x7_up)
        x8_up = self.block_eight_up(x8)
        x8_up = x8_up + x1
        x9 = self.block_nine(x8_up)
        # x9 = F.dropout3d(x9, p=0.5, training=True)

        out = self.out_conv(x9)
        return out


    def forward(self, input):

        features = self.encoder(input)
        out = self.decoder(features)

        return out


if __name__ == "__main__":
    params = {'in_chns': 1,
              'class_num': 1,
              'feature_chns': [16, 32, 64, 128, 256],
              'dropout': [0, 0, 0, 0.5, 0.5],
              'trilinear': False,
              'dropout': [0, 0, 0.3, 0.4, 0.5],
              'dataset_name': 'heart'}
    Net = VNet(params)
    Net = Net.double()
    print('net total parameters:', sum(param.numel() for param in Net.parameters()))

    x = np.random.rand(4, 1, 96, 96, 96)
    xt = torch.from_numpy(x)
    xt = torch.tensor(xt)

    y = Net(xt)
    y = y.detach().numpy()
    print(y.shape)

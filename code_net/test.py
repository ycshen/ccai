import torch


class A:
    def __init__(self) -> None:
        self.window_sizes = (1, 2, 3)
    
    def test(self):
        coords_h = torch.arange(self.window_sizes[0])
        coords_w = torch.arange(self.window_sizes[1])
        coords_d = torch.arange(self.window_sizes[2])
        coords = torch.stack(torch.meshgrid(
            [coords_h, coords_w, coords_d], indexing='ij'))  # (3, Wh, Ww, Wd), coordinate xyz of a pixel 
        coords_flatten = torch.flatten(coords, 1)  # 3, Wh*Ww*Wd
        print('coords_flatten.shape', coords_flatten.shape)
        # The distance of every element to every element in a window in 3 dimensions (HWD)
        relative_coords = coords_flatten[:, :, None] - \
            coords_flatten[:, None, :]  # 3, Wh*Ww*Wd, Wh*Ww*Wd
        relative_coords = relative_coords.permute(
            1, 2, 0).contiguous()  # Wh*Ww*Wd, Wh*Ww*Wd, 2
        print('relative_coords.shape', relative_coords.shape)
        relative_coords[:, :, 0] += self.window_sizes[0] - 1  # shift to start from 0
        relative_coords[:, :, 1] += self.window_sizes[1] - 1
        relative_coords[:, :, 2] += self.window_sizes[2] - 1
        # relative_coords[:, :, 0] *= (2 * self.window_sizes[1] - 1)
        relative_coords[:, :, 0] *= (2 * self.window_sizes[1] - 1) * (2 * self.window_sizes[2] - 1)
        relative_coords[:, :, 1] *= 2 * self.window_sizes[2] - 1
        relative_position_index = relative_coords.sum(-1)  # Wh*Ww, Wh*Ww
        print(relative_position_index)

if __name__ == '__main__':
    a = A()
    a.test()
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import torch
import torch.nn as nn
import numpy as np

class ConvBlock(nn.Module):
    """two convolution layers with batch norm and leaky relu"""
    def __init__(self,in_channels, out_channels, dropout_p):
        """
        dropout_p: probability to be zeroed
        """
        super(ConvBlock, self).__init__()
        self.conv_conv = nn.Sequential(
            nn.Conv3d(in_channels, out_channels, kernel_size=3, padding=1),
            # nn.BatchNorm3d(out_channels),
            nn.LeakyReLU(),
            nn.Dropout(dropout_p),
            nn.Conv3d(out_channels, out_channels, kernel_size=3, padding=1),
            # nn.BatchNorm3d(out_channels),
            nn.LeakyReLU()
        )
       
    def forward(self, x):
        return self.conv_conv(x)

class DownBlock(nn.Module):
    """Downsampling followed by ConvBlock"""
    def __init__(self, in_channels, out_channels, dropout_p):
        super(DownBlock, self).__init__()
        self.maxpool_conv = nn.Sequential(
            nn.MaxPool3d(2),
            # nn.Conv3d(in_channels, out_channels, 2, 2),
            # nn.LeakyReLU(),
            # ConvBlock(out_channels, out_channels, dropout_p)
            ConvBlock(in_channels, out_channels, dropout_p)
        )

    def forward(self, x):
        return self.maxpool_conv(x)

class UpBlock(nn.Module):
    """Upssampling followed by ConvBlock"""
    def __init__(self, in_channels1, in_channels2, out_channels, dropout_p,
                 trilinear=False):
        super(UpBlock, self).__init__()
        self.trilinear = trilinear
        if trilinear:
            self.conv1x1 = nn.Conv3d(in_channels1, in_channels2, kernel_size = 1)
            self.up = nn.Upsample(scale_factor=2, mode='trilinear', align_corners=True)
        else:
            self.up = nn.ConvTranspose3d(in_channels1, in_channels2, kernel_size=2, stride=2)
        self.conv = ConvBlock(in_channels2 * 2, out_channels, dropout_p)

    def forward(self, x1, x2):
        if self.trilinear:
            x1 = self.conv1x1(x1)
        x1 = self.up(x1)
        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)





class UNet3D(nn.Module):
    def __init__(self, params):
        super(UNet3D, self).__init__()
        self.params = params
        self.in_chns = self.params['in_chns']
        self.ft_chns = self.params['feature_chns']
        self.n_class = self.params['class_num']
        self.trilinear = self.params['trilinear']
        self.dropout = self.params['dropout']
        self.dataset_name = self.params['dataset_name']
        assert(len(self.ft_chns) == 5 or len(self.ft_chns) == 4)

        self.in_conv= ConvBlock(self.in_chns, self.ft_chns[0], self.dropout[0])
        self.down1  = DownBlock(self.ft_chns[0], self.ft_chns[1], self.dropout[1])
        self.down2  = DownBlock(self.ft_chns[1], self.ft_chns[2], self.dropout[2])
        self.down3  = DownBlock(self.ft_chns[2], self.ft_chns[3], self.dropout[3])
        if(len(self.ft_chns) == 5):
          self.down4  = DownBlock(self.ft_chns[3], self.ft_chns[4], self.dropout[4])
          self.up1 = UpBlock(self.ft_chns[4], self.ft_chns[3], self.ft_chns[3], 
               dropout_p = 0.0, trilinear=self.trilinear) 
        self.up2 = UpBlock(self.ft_chns[3], self.ft_chns[2], self.ft_chns[2], 
               dropout_p = 0.0, trilinear=self.trilinear) 
        self.up3 = UpBlock(self.ft_chns[2], self.ft_chns[1], self.ft_chns[1], 
               dropout_p = 0.0, trilinear=self.trilinear) 
        self.up4 = UpBlock(self.ft_chns[1], self.ft_chns[0], self.ft_chns[0], 
               dropout_p = 0.0, trilinear=self.trilinear)

        if self.dataset_name == 'heart':
            self.out_conv = nn.Sequential(
                nn.Conv3d(self.ft_chns[0], self.n_class, 1, padding=0),
                nn.Sigmoid()
            )
        else:
            self.out_conv = nn.Sequential(
                nn.Conv3d(self.ft_chns[0], int(self.ft_chns[0] / 2), kernel_size=3, padding=1),
                nn.LeakyReLU(),
                nn.ConvTranspose3d(int(self.ft_chns[0] / 2), self.n_class, (1, 2, 2), (1, 2, 2)),
                nn.Sigmoid()
            )




    def forward(self, x):
        x0 = self.in_conv(x) #[B, C, D, W, H] = [1, 16, 48, 256, 256]
        x1 = self.down1(x0)
        x2 = self.down2(x1)
        x3 = self.down3(x2)
        if(len(self.ft_chns) == 5):
          x4 = self.down4(x3)
          x = self.up1(x4, x3)
        else:
          x = x3
        x = self.up2(x, x2)
        x = self.up3(x, x1)
        x = self.up4(x, x0)
        output = self.out_conv(x)

        return output

if __name__ == "__main__":
    params = {'in_chns': 1,
              'class_num': 1,
              'feature_chns': [16, 32, 64, 128, 256],
              'dropout': [0, 0, 0, 0.5, 0.5],
              'trilinear': False}
    Net = UNet3D(params)
    Net = Net.double()
    print('net total parameters:', sum(param.numel() for param in Net.parameters()))

    x = np.random.rand(4, 1, 96, 96, 96)
    xt = torch.from_numpy(x)
    xt = torch.tensor(xt)
    
    y = Net(xt)
    y = y.detach().numpy()
    print(y.shape)

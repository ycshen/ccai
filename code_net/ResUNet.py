from __future__ import print_function, division

import torch
import torch.nn as nn
import numpy as np


class ConvBlock(nn.Module):
    """two convolution layers with batch norm and leaky relu"""

    def __init__(self, in_channels, out_channels, dropout_p):
        """
        dropout_p: probability to be zeroed
        """
        super(ConvBlock, self).__init__()
        self.conv_conv = nn.Sequential(
            nn.Conv3d(in_channels, out_channels, kernel_size=3, padding=1),
            # nn.BatchNorm3d(out_channels),
            nn.LeakyReLU(),
            nn.Dropout(dropout_p),
            nn.Conv3d(out_channels, out_channels, kernel_size=3, padding=1),
            # nn.BatchNorm3d(out_channels),
            nn.LeakyReLU()
        )

    def forward(self, x):
        return self.conv_conv(x)

class DownBlock(nn.Module):
    """Downsampling followed by ConvBlock"""
    def __init__(self, in_channels, out_channels):
        super(DownBlock, self).__init__()
        self.maxpool_conv = nn.Sequential(
            nn.Conv3d(in_channels, out_channels, 2, 2),
            nn.LeakyReLU()
        )

    def forward(self, x):
        return self.maxpool_conv(x)

class UpBlock(nn.Module):
    """Upssampling followed by ConvBlock"""
    def __init__(self, in_channels1, in_channels2, trilinear=False):
        super(UpBlock, self).__init__()
        self.trilinear = trilinear
        if trilinear:
            self.conv1x1 = nn.Conv3d(in_channels1, in_channels2, kernel_size = 1)
            self.up = nn.Upsample(scale_factor=2, mode='trilinear', align_corners=True)
        else:
            self.up = nn.ConvTranspose3d(in_channels1, in_channels2, kernel_size=2, stride=2)

    def forward(self, x1):
        if self.trilinear:
            x1 = self.conv1x1(x1)
        x1 = self.up(x1)
        return x1

class ResUNet(nn.Module):
    def __init__(self, params):
        super(ResUNet, self).__init__()
        self.params = params
        self.in_chns = self.params['in_chns']
        self.ft_chns = self.params['feature_chns']
        self.n_class = self.params['class_num']
        self.trilinear = self.params['trilinear']
        self.dropout = self.params['dropout']
        self.dataset_name = self.params['dataset_name']
        assert(len(self.ft_chns) == 5 or len(self.ft_chns) == 4)

        self.in_conv= ConvBlock(self.in_chns, self.ft_chns[0], self.dropout[0])
        self.down1  = DownBlock(self.ft_chns[0], self.ft_chns[1])
        self.conv1  = ConvBlock(self.ft_chns[1], self.ft_chns[1], self.dropout[1])
        self.down2  = DownBlock(self.ft_chns[1], self.ft_chns[2])
        self.conv2  = ConvBlock(self.ft_chns[2], self.ft_chns[2], self.dropout[2])
        self.down3  = DownBlock(self.ft_chns[2], self.ft_chns[3])
        self.conv3  = ConvBlock(self.ft_chns[3], self.ft_chns[3], self.dropout[3])
        if(len(self.ft_chns) == 5):
          self.down4  = DownBlock(self.ft_chns[3], self.ft_chns[4])
          self.conv4  = ConvBlock(self.ft_chns[4], self.ft_chns[4], self.dropout[4])
          self.up1 = UpBlock(self.ft_chns[4], self.ft_chns[3], trilinear=self.trilinear)
          self.conv5 = ConvBlock(self.ft_chns[3]*2, self.ft_chns[3], dropout_p = 0.0)
        self.up2 = UpBlock(self.ft_chns[3], self.ft_chns[2], trilinear=self.trilinear)
        self.conv6 = ConvBlock(self.ft_chns[2]*2, self.ft_chns[2], dropout_p = 0.0)
        self.up3 = UpBlock(self.ft_chns[2], self.ft_chns[1], trilinear=self.trilinear)
        self.conv7 = ConvBlock(self.ft_chns[1]*2, self.ft_chns[1], dropout_p = 0.0)
        self.up4 = UpBlock(self.ft_chns[1], self.ft_chns[0], trilinear=self.trilinear)
        self.conv8 = ConvBlock(self.ft_chns[0]*2, self.ft_chns[0], dropout_p = 0.0)
        if self.dataset_name == 'heart':
            self.out_conv = nn.Sequential(
                nn.Conv3d(self.ft_chns[0], self.n_class, 1, padding=0),
                nn.Sigmoid()
            )
        else:
            self.out_conv = nn.Sequential(
                nn.Conv3d(self.ft_chns[0], int(self.ft_chns[0] / 2), kernel_size=3, padding=1),
                nn.LeakyReLU(),
                nn.ConvTranspose3d(int(self.ft_chns[0] / 2), self.n_class, (1, 2, 2), (1, 2, 2)),
                nn.Sigmoid()
            )


    def forward(self, x):
        x0 = self.in_conv(x)
        x1 = self.down1(x0)
        x2 = self.conv1(x1) + x1
        x3 = self.down2(x2)
        x4 = self.conv2(x3) + x3
        x5 = self.down3(x4)
        x6 = self.conv3(x5) + x5
        x7 = self.down4(x6)
        x8 = self.conv4(x7) + x7

        x9 = self.up1(x8)
        x10 = self.conv5(torch.cat([x9, x6], dim=1)) + x9
        x11 = self.up2(x10)
        x12 = self.conv6(torch.cat([x11, x4], dim=1)) + x11
        x13 = self.up3(x12)
        x14 = self.conv7(torch.cat([x13, x2], dim=1)) + x13
        x15 = self.up4(x14)
        x16 = self.conv8(torch.cat([x15, x0], dim=1)) + x15

        output = self.out_conv(x16)
        return output


if __name__ == "__main__":
    params = {'in_chns': 1,
              'class_num': 1,
              'feature_chns': [16, 32, 64, 128, 256],
              'dropout': [0, 0, 0, 0.5, 0.5],
              'trilinear': False}
    Net = ResUNet(params)
    Net = Net.double()
    print('net total parameters:', sum(param.numel() for param in Net.parameters()))

    x = np.random.rand(4, 1, 96, 96, 96)
    xt = torch.from_numpy(x)
    xt = torch.tensor(xt)

    y = Net(xt)
    y = y.detach().numpy()
    print(y.shape)
# coding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import copy
import logging
import math

from os.path import join as pjoin

import torch
import torch.nn as nn
import numpy as np

from torch.nn import CrossEntropyLoss, Dropout, Softmax, Linear, Conv2d, LayerNorm
from torch.nn.modules.utils import _pair
from scipy import ndimage
from .swin_transformer_unet_skip_expand_decoder_sys import SwinTransformerSys

logger = logging.getLogger(__name__)

class SwinUnet(nn.Module):
    def __init__(self, params):
        super(SwinUnet, self).__init__()
        self.params = params
        self.img_size = self.params['img_size']
        self.patch_size = self.params['patch_size']
        self.in_chans = self.params['in_chans']
        self.num_classes = self.params['num_classes']
        self.embed_dim = self.params['embed_dim']
        self.depths = self.params['depths']
        self.num_heads = self.params['num_heads']
        self.window_size = self.params['window_size']
        self.mlp_ratio = self.params['mlp_ratio']
        self.qkv_bias = self.params['qkv_bias']
        self.qk_scale = self.params['qk_scale']
        self.drop_rate = self.params['drop_rate']
        self.drop_path_rate = self.params['drop_path_rate']
        self.ape = self.params['ape']
        self.patch_norm = self.params['patch_norm']
        self.use_checkpoint = self.params['use_checkpoint']
        self.is_val = self.params['is_val']

        self.swin_unet = SwinTransformerSys(img_size=self.img_size,
                                patch_size=self.patch_size,
                                in_chans=self.in_chans,
                                num_classes=self.num_classes,
                                embed_dim=self.embed_dim,
                                depths=self.depths,
                                num_heads=self.num_heads,
                                window_size=self.window_size,
                                mlp_ratio=self.mlp_ratio,
                                qkv_bias=self.qkv_bias,
                                qk_scale=self.qk_scale,
                                drop_rate=self.drop_rate,
                                drop_path_rate=self.drop_path_rate,
                                ape=self.ape,
                                patch_norm=self.patch_norm,
                                use_checkpoint=self.use_checkpoint)

    def forward(self, x):
        # if x.size()[1] == 1:
        #     x = x.repeat(1,3,1,1)
        x_shape = list(x.shape)
        if (len(x_shape) == 5):
            [N, C, H, W, D] = x_shape
            new_shape = [N * D, C, H, W]
            x = torch.transpose(x, 1, 4)  # [N, D, H, W, C]
            x = torch.transpose(x, 2, 4)  # [N, D, C, W, H]
            x = torch.transpose(x, 3, 4)  # [N, D, C, H, W]
            x = torch.reshape(x, new_shape)
        m = nn.ZeroPad2d(56)
        x = m(x)
        output = self.swin_unet(x)
        if self.is_val:
            output = output[:, :, 56:168, 56: 168]


        if (len(x_shape) == 5):
            new_shape = [N, D] + list(output.shape)[1:] #[N, D, C, H, W]
            output = torch.reshape(output, new_shape) #[N, D, C, H, W]
            output = torch.transpose(output, 1, 2) #[N, C, D, H, W]
            output = torch.transpose(output, 2, 4)  # [N, C, W, H, D]
            output = torch.transpose(output, 2, 3)  # [N, C, H, W, D]
        return output


 
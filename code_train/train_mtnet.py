import sys
sys.path.append("..")
import os
from time import time

import numpy as np

import torch
import torch.backends.cudnn as cudnn
from torch.utils.data import DataLoader
import torch.nn as nn

from dataset.mydataset import MyDataset

from code_net.axialnet import gated
from code_loss.Dice import DiceLoss
from code_loss.ELDice import ELDiceLoss
from code_loss.WBCE import WCELoss
from code_loss.Jaccard import JaccardLoss
from code_loss.SS import SSLoss
from code_loss.Tversky import TverskyLoss
from code_loss.Hybrid import HybridLoss
from code_loss.BCE import BCELoss
import random
import code_utilities.parameter as para
from torchvision import transforms
from dataset.read_data import RandomCrop, CenterCrop, RandomRotFlip, ToTensor

# dataset_name = sys.argv[1]
dataset_name = 'heart'

train_txt_name = 'train.txt'
val_txt_name = 'val.txt'
save_pth_name = '/data/ztl/CCAI/result_modules/module_mtnet/' + dataset_name + '/'

os.environ['CUDA_VISIBLE_DEVICES'] = '4'

def setup_seed(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)  # 为了禁止hash随机化，使得实验可复现
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)  # if you are using multi-GPU.
    torch.backends.cudnn.benchmark = False
    # torch.backends.cudnn.enabled = False
    torch.backends.cudnn.deterministic = True
setup_seed(1092)

def _init_fn(worker_id):
    random.seed(10 + worker_id)
    np.random.seed(10 + worker_id)
    torch.manual_seed(10 + worker_id)
    torch.cuda.manual_seed(10 + worker_id)
    torch.cuda.manual_seed_all(10 + worker_id)

def init(module):
    if isinstance(module, nn.Conv3d) or isinstance(module, nn.ConvTranspose3d):
        nn.init.kaiming_normal_(module.weight.data, 0.25)
        nn.init.constant_(module.bias.data, 0)

params = {'in_chans': 1,
              'is_val':False,
              'img_size': 128,
              'patch_size': 4,
              'num_classes': 1,
              'embed_dim': 96,
              'depths': [2, 2, 2, 2],
              'num_heads': [1, 4, 4, 8, 16], #1231
              'window_size': 7,
              'qkv_bias': True,
              'qk_scale': None,
              'drop_rate': 0.,
              'attn_drop_rate': 0.,
              'drop_path_rate': 0.1,
              'mlp_ratio': 4.,
              'use_checkpoint': False,
              'ape': False,
              'patch_norm': True
              }

net = gated(img_size = params['img_size'], imgchan = params['in_chans'], is_val=params['is_val'])
net.apply(init)
# net = net.cuda()
# 计算网络参数
print('net total parameters:', sum(param.numel() for param in net.parameters()))

# 设置显卡相关

# cudnn.benchmark = para.cudnn_benchmark

# 定义网络
net = torch.nn.DataParallel(net).cuda()
device = torch.device('cuda:0')
net.to(device)
# 定义Dateset
train_ds = MyDataset(os.path.join(para.txt_pth+dataset_name, train_txt_name), dataset_name, transform = transforms.Compose([
                          RandomRotFlip(),
                          RandomCrop((112, 112, 80)),
                          ToTensor(),
                          ]))
val_ds = MyDataset(os.path.join(para.txt_pth+dataset_name, val_txt_name), dataset_name, transform = transforms.Compose([
                           CenterCrop((112, 112, 80)),
                           ToTensor()
                       ]))

# 定义数据加载
train_dl = DataLoader(train_ds, para.batch_size, num_workers=para.num_workers, pin_memory=para.pin_memory, worker_init_fn=_init_fn)
val_dl = DataLoader(val_ds, para.batch_size, num_workers=para.num_workers, pin_memory=para.pin_memory, worker_init_fn=_init_fn)

# 挑选损失函数
loss_func_list = [DiceLoss(), ELDiceLoss(), WCELoss(), JaccardLoss(), SSLoss(), TverskyLoss(), HybridLoss(), BCELoss()]
loss_func = loss_func_list[0]
# dice = Dice()

# 定义优化器
opt = torch.optim.Adam(net.parameters(), lr=para.learning_rate)

# 学习率衰减
lr_decay = torch.optim.lr_scheduler.MultiStepLR(opt, para.learning_rate_decay)

# 深度监督衰减系数
alpha = para.alpha

# 训练网络
start = time()
tmp_val_loss = 100000
tmp_val_dice = 0
mm = nn.ZeroPad2d(8)
for epoch in range(para.Epoch):

    lr_decay.step()

    net.train()

    for step, (ct, seg) in enumerate(train_dl):

        ct = ct.cuda()
        seg = seg.cuda()


        outputs = net(ct)
        [N, H, W, D] = seg.shape
        seg = torch.transpose(seg, 1, 3)  # [N, D, W, H]
        seg = torch.transpose(seg, 2, 3)  # [N, D, H, W]
        seg = mm(seg) # [N, D, H, W]
        seg = torch.transpose(seg, 1, 3)  # [N, W, H, D]
        seg = torch.transpose(seg, 1, 2)  # [N, H, W, D]

        loss = loss_func(outputs, seg)


        # dice_train = dice(outputs, seg)

        opt.zero_grad()
        loss.backward()
        opt.step()

        if step % 5 is 0:
            print('epoch:{}, step:{}, loss:{:.3f},time:{:.3f} min'.format(epoch, step, loss.item(), (time() - start) / 60))

torch.save(net.state_dict(), save_pth_name + 'net.pth')
    # # 保存模型
    # if epoch % 10 is 0 and epoch is not 0:
    #     val_loss_mean = []
    #     # val_dice_mean = []
    #     with torch.no_grad():
    #         net.eval()
    #         for step, (ct, seg) in enumerate(val_dl):
    #             ct = ct.cuda()
    #             seg = seg.cuda()
    #             val_output = net(ct)
    #             val_loss = loss_func(val_output, seg)
    #             # val_dice = dice(val_output, seg)
    #             val_loss_mean.append(val_loss.item())
    #             # val_dice_mean.append(val_dice.item())
    #     val_loss_mean = sum(val_loss_mean) / len(val_loss_mean)
    #     # val_dice_mean = sum(val_dice_mean) / len(val_dice_mean)
    #     torch.save(net.state_dict(), save_pth_name + 'net{}-{:.3f}.pth'.format(epoch, val_loss_mean))
    #     print('epoch:{}, val_loss:{:.3f}'.format(epoch, val_loss_mean))
    #     # if val_loss_mean < tmp_val_loss:
    #     #     tmp_val_loss = val_loss_mean
    #     #     # tmp_val_dice = val_dice_mean
    #     #     # 网络模型的命名方式为：epoch轮数+当前minibatch的loss+本轮epoch的平均loss
    #     #     torch.save(net.state_dict(), save_pth_name+'net{}-{:.3f}.pth'.format(epoch, val_loss_mean))






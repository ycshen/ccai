import sys

sys.path.append("..")
import os
import random
from time import time
import pathlib

import code_utilities.parameter as para
import numpy as np
import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn
from code_loss.BCE import BCELoss
from code_loss.Dice import DiceLoss
from code_loss.ELDice import ELDiceLoss
from code_loss.Hybrid import HybridLoss
from code_loss.Jaccard import JaccardLoss
from code_loss.SS import SSLoss
from code_loss.Tversky import TverskyLoss
from code_loss.WBCE import WCELoss
from code_net.CTNet import ConvTransformer
from code_val.heart_test import calculate_metric_percase
from dataset.mydataset import MyDataset
from dataset.read_data import CenterCrop, RandomCrop, RandomRotFlip, ToTensor
from torch.utils.data import DataLoader
from torchvision import transforms

dataset_name = sys.argv[1]

train_txt_name = 'train.txt'
val_txt_name = 'val.txt'
overfit_txt_name = 'overfit.txt'
save_pth_name = '/data/ztl/CCAI/result_modules/model_ctnet/' + dataset_name + '/'

def setup_seed(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)  # 为了禁止hash随机化，使得实验可复现
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)  # if you are using multi-GPU.
    torch.backends.cudnn.benchmark = False
    # torch.backends.cudnn.enabled = False
    torch.backends.cudnn.deterministic = True
setup_seed(1092)

def _init_fn(worker_id):
    random.seed(10 + worker_id)
    np.random.seed(10 + worker_id)
    torch.manual_seed(10 + worker_id)
    torch.cuda.manual_seed(10 + worker_id)
    torch.cuda.manual_seed_all(10 + worker_id)

def init(module):
    if isinstance(module, nn.Conv3d) or isinstance(module, nn.ConvTranspose3d):
        nn.init.kaiming_normal_(module.weight.data, 0.25)
        nn.init.constant_(module.bias.data, 0)

config = {
    'task': 'overfit', # ['train', 'overfit']
    'in_channels': 1,
    'in_resolution': (112, 112, 80),
    'base_channels': 8,
    'position_dropout_rate': 0.1,
    'num_layers_in_transformer_block': 2,
    'num_class': 1,
    'dataset_name': dataset_name,
    'eval_every_n_epochs': 50,
    'print_every_n_steps': 1
}


def train(net, train_dataloader, val_dataloader, device, config):
    # 挑选损失函数
    loss_func_list = [DiceLoss(), ELDiceLoss(), WCELoss(), JaccardLoss(), SSLoss(), TverskyLoss(), HybridLoss(), BCELoss()]
    loss_func = loss_func_list[0]

    # 定义优化器
    opt = torch.optim.Adam(net.parameters(), lr=para.learning_rate)

    # 学习率衰减
    lr_decay = torch.optim.lr_scheduler.MultiStepLR(opt, para.learning_rate_decay)

    # 深度监督衰减系数
    alpha = para.alpha
    
    # 训练网络
    start = time()

    train_loss_running = 0
    num_samples_running = 0
    for epoch in range(para.Epoch):
        net.train()
        if epoch > 0:
            lr_decay.step()
        for step, (ct, seg) in enumerate(train_dataloader):

            ct = ct.to(device)
            seg = seg.to(device)
            outputs = net(ct)
            loss = loss_func(outputs, seg)
            # dice_train = dice(outputs, seg)
            train_loss_running += loss.item()
            num_samples_running += ct.shape[0] # num_samples += batch_size
            opt.zero_grad()
            loss.backward()
            opt.step()

            if step % config['print_every_n_steps'] == config['print_every_n_steps'] - 1:
                print('epoch:{}, step:{}, loss:{:.3f},time:{:.3f} min'.format(epoch, step, train_loss_running / num_samples_running, (time() - start) / 60))
                train_loss_running = 0
                num_samples_running = 0

        if epoch % config['eval_every_n_epochs'] == config['eval_every_n_epochs']-1:
            net.eval()
            loss_val = 0
            eval_results = {'dice':0, 'jc': 0, 'hd': 0, 'asd': 0}
            for ct, seg in val_dataloader:
                ct = ct.to(device)
                seg = seg.to(device)
                with torch.no_grad():
                    seg_pred = net(ct)
                    loss_val += loss_func(seg_pred, seg)
                    # calculate_metric_percase accepts Numpy array
                    dice, jc, hd, asd = calculate_metric_percase(seg_pred[0].cpu().numpy(), seg.cpu().numpy())
                    eval_results['dice'] += dice
                    eval_results['jc'] += jc
                    eval_results['hd'] += hd
                    eval_results['asd'] += asd
            for k in eval_results.keys():
                eval_results[k] /= len(val_dataloader)
            loss_val /= len(val_dataloader)
            print(f"Eval :{eval_results}; loss: {loss_val.item()}")


def main(config):
    net = ConvTransformer(
        in_channels=config['in_channels'],
        in_resolution=config['in_resolution'],
        base_channels=config['base_channels'],
        position_dropout_rate=config['position_dropout_rate'],
        num_layers=config['num_layers_in_transformer_block']
    )
    net.apply(init)

    # 计算网络参数
    print('net total parameters:', sum(param.numel() for param in net.parameters()))

    net = torch.nn.DataParallel(net)
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    net.to(device)

    train_dataset = MyDataset(
        os.path.join(para.txt_pth+dataset_name, overfit_txt_name if config['task']=='overfit' else train_txt_name), 
        dataset_name, 
        transform = transforms.Compose(
            [
                RandomRotFlip(),
                RandomCrop((112, 112, 80)),
                ToTensor(),
            ]
        )
    )
    train_dataloader = DataLoader(
        train_dataset, 
        para.batch_size, 
        num_workers=para.num_workers, 
        pin_memory=para.pin_memory, 
        worker_init_fn=_init_fn
    )
    val_dataset = MyDataset(
        os.path.join(para.txt_pth+dataset_name, val_txt_name), 
        dataset_name, 
        transform = transforms.Compose(
            [
                CenterCrop((112, 112, 80)),
                ToTensor()
            ]
        )
    )    
    val_dataloader = DataLoader(
        val_dataset,
        para.batch_size, 
        num_workers=para.num_workers, 
        pin_memory=para.pin_memory, 
        worker_init_fn=_init_fn
    )

    train(net, train_dataloader, val_dataloader, device, config)

    pathlib.Path(save_pth_name).mkdir(parents=True, exist_ok=True)
    torch.save(net.state_dict(), save_pth_name + 'net.pth')


if __name__ == '__main__':
    main(config)

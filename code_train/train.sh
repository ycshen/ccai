#!/bin/bash
dataset_name="heart"
file_name="/train_out"
path_name="/data/ztl/CCAI/result_log-out/"
v_log_path=$path_name"log_v/"$dataset_name$file_name
u25_log_path=$path_name"log_u25/"$dataset_name$file_name
unr_log_path=$path_name"log_unr/"$dataset_name$file_name
resu_log_path=$path_name"log_resu/"$dataset_name$file_name
scse_log_path=$path_name"log_scse/"$dataset_name$file_name
ulight_log_path=$path_name"log_ulight/"$dataset_name$file_name
ffc_log_path=$path_name"log_ffc/"$dataset_name$file_name
u2d_log_path=$path_name"log_u2d/"$dataset_name$file_name
attention_log_path=$path_name"log_attention/"$dataset_name$file_name
scse2d_log_path=$path_name"log_scse2d/"$dataset_name$file_name
cople_log_path=$path_name"log_cople/"$dataset_name$file_name
nest_log_path=$path_name"log_nest/"$dataset_name$file_name
ffc1_log_path=$path_name"log_ffc_1/"$dataset_name$file_name
u2d1_log_path=$path_name"log_u2d_1/"$dataset_name$file_name
attention1_log_path=$path_name"log_attention_1/"$dataset_name$file_name
scse2d1_log_path=$path_name"log_scse2d_1/"$dataset_name$file_name
cople1_log_path=$path_name"log_cople_1/"$dataset_name$file_name
nest1_log_path=$path_name"log_nest_1/"$dataset_name$file_name
ffc2_log_path=$path_name"log_ffc_2/"$dataset_name$file_name
u2d2_log_path=$path_name"log_u2d_2/"$dataset_name$file_name
attention2_log_path=$path_name"log_attention_2/"$dataset_name$file_name
scse2d2_log_path=$path_name"log_scse2d_2/"$dataset_name$file_name
cople2_log_path=$path_name"log_cople_2/"$dataset_name$file_name
nest2_log_path=$path_name"log_nest_2/"$dataset_name$file_name
nest3_log_path=$path_name"log_nest_3/"$dataset_name$file_name
ffc02_log_path=$path_name"log_ffc_02/"$dataset_name$file_name
ffc12_log_path=$path_name"log_ffc_12/"$dataset_name$file_name
ffc22_log_path=$path_name"log_ffc_22/"$dataset_name$file_name
nest4_log_path=$path_name"log_nest_4/"$dataset_name$file_name
nest5_log_path=$path_name"log_nest_5/"$dataset_name$file_name
stacking3_log_path=$path_name"log_stacking_3/"$dataset_name$file_name
stacking4_log_path=$path_name"log_stacking_4/"$dataset_name$file_name
stacking5_log_path=$path_name"log_stacking_5/"$dataset_name$file_name
ct_log_path=$path_name"log_ctnet/"$dataset_name$file_name
nest6_log_path=$path_name"log_nest_6/"$dataset_name$file_name
nest7_log_path=$path_name"log_nest_7/"$dataset_name$file_name
nest8_log_path=$path_name"log_nest_8/"$dataset_name$file_name
cat1_log_path=$path_name"log_cat_1/"$dataset_name$file_name
cat2_log_path=$path_name"log_cat_2/"$dataset_name$file_name
cat3_log_path=$path_name"log_cat_3/"$dataset_name$file_name
swin_log_path=$path_name"log_swin/"$dataset_name$file_name
transunet_log_path=$path_name"log_transunet/"$dataset_name$file_name
utnet_log_path=$path_name"log_utnet/"$dataset_name$file_name
mtnet_log_path=$path_name"log_mtnet/"$dataset_name$file_name
#nohup python -u train_v.py $dataset_name > $v_log_path 2>&1 &
#nohup python -u train_u25.py $dataset_name > $u25_log_path 2>&1 &
#nohup python -u train_resu.py $dataset_name > $resu_log_path 2>&1 &
#nohup python -u train_scse.py $dataset_name > $scse_log_path 2>&1 &
#nohup python -u train_ulight.py $dataset_name > $ulight_log_path 2>&1 &
#nohup python -u train_ffc.py $dataset_name > $ffc_log_path 2>&1 &
#nohup python -u train_u2d.py $dataset_name > $u2d_log_path 2>&1 &
#nohup python -u train_attention.py $dataset_name > $attention_log_path 2>&1 &
#nohup python -u train_nest.py $dataset_name > $nest_log_path 2>&1 &
#nohup python -u train_cople.py $dataset_name > $cople_log_path 2>&1 &
#nohup python -u train_scse2d.py $dataset_name > $scse2d_log_path 2>&1 &
#nohup python -u train_ffc_1.py $dataset_name > $ffc1_log_path 2>&1 &
#nohup python -u train_u2d_1.py $dataset_name > $u2d1_log_path 2>&1 &
#nohup python -u train_attention_1.py $dataset_name > $attention1_log_path 2>&1 &
#nohup python -u train_nest_1.py $dataset_name > $nest1_log_path 2>&1 &
#nohup python -u train_cople_1.py $dataset_name > $cople1_log_path 2>&1 &
#nohup python -u train_scse2d_1.py $dataset_name > $scse2d1_log_path 2>&1 &
#nohup python -u train_ffc_2.py $dataset_name > $ffc2_log_path 2>&1 &
#nohup python -u train_u2d_2.py $dataset_name > $u2d2_log_path 2>&1 &
#nohup python -u train_attention_2.py $dataset_name > $attention2_log_path 2>&1 &
#nohup python -u train_nest_2.py $dataset_name > $nest2_log_path 2>&1 &
#nohup python -u train_cople_2.py $dataset_name > $cople2_log_path 2>&1 &
#nohup python -u train_scse2d_2.py $dataset_name > $scse2d2_log_path 2>&1 &
#nohup python -u train_nest_3.py $dataset_name > $nest3_log_path 2>&1 &
#nohup python -u train_ffc_02.py $dataset_name > $ffc02_log_path 2>&1 &
#nohup python -u train_ffc_12.py $dataset_name > $ffc12_log_path 2>&1 &
#nohup python -u train_ffc_22.py $dataset_name > $ffc22_log_path 2>&1 &
#nohup python -u train_nest_4.py $dataset_name > $nest4_log_path 2>&1 &
#nohup python -u train_nest_5.py $dataset_name > $nest5_log_path 2>&1 &
#nohup python -u train_stacking_3.py $dataset_name > $stacking3_log_path 2>&1 &
#nohup python -u train_stacking_4.py $dataset_name > $stacking4_log_path 2>&1 &
#nohup python -u train_stacking_5.py $dataset_name > $stacking5_log_path 2>&1 &
#mkdir -p $path_name"log_ctnet/"$dataset_name
#CUDA_VIDIBLE_DEVICES=4,5 nohup python train_ct.py $dataset_name > $ct_log_path 2>&1 &
#nohup python -u train_nest_6.py $dataset_name > $nest6_log_path 2>&1 &
#nohup python -u train_nest_7.py $dataset_name > $nest7_log_path 2>&1 &
#nohup python -u train_nest_8.py $dataset_name > $nest8_log_path 2>&1 &
#nohup python -u train_cat_1.py $dataset_name > $cat1_log_path 2>&1 &
#nohup python -u train_cat_2.py $dataset_name > $cat2_log_path 2>&1 &
#nohup python -u train_cat_3.py $dataset_name > $cat3_log_path 2>&1 &
#nohup python -u train_swin.py $dataset_name > $swin_log_path 2>&1 &
#nohup python -u train_transunet.py $dataset_name > $transunet_log_path 2>&1 &
#nohup python -u train_utnet.py $dataset_name > $utnet_log_path 2>&1 &
nohup python -u train_mtnet.py $dataset_name > $mtnet_log_path 2>&1 &

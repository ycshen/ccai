import sys
import argparse

sys.path.append("..")
import os
import random
from time import time
from pathlib import Path
from tqdm import tqdm

import code_utilities.parameter as para
import numpy as np
import torch
import torch.nn as nn
from code_loss.BCE import BCELoss
from code_loss.Dice import DiceLoss
from code_loss.ELDice import ELDiceLoss
from code_loss.Hybrid import HybridLoss
from code_loss.Jaccard import JaccardLoss
from code_loss.SS import SSLoss
from code_loss.Tversky import TverskyLoss
from code_loss.WBCE import WCELoss
from code_net.CCATNet import CATNet
from dataset.mydataset import MyDataset
from dataset.read_data import CenterCrop, RandomCrop, RandomRotFlip, ToTensor
from torch.utils.data import DataLoader
from torchvision import transforms


parser = argparse.ArgumentParser(description='train ccat model')
parser.add_argument('--task', type=str, default='train', choices=['train', 'pretrain'])
parser.add_argument('--dataset', type=str, default='heart', choices=['heart', 'liver', 'tumor'])
parser.add_argument('--pretrain-weight', type=str, default=None, help='path to pretrained weight, optional')
parser.add_argument('--cuda-visible-devices', type=str, default='5', help="comma separated numbers, i.e. '3,4,5'")
parser.add_argument('--pretrain-epochs', type=int, default=100)
args = parser.parse_args()

os.environ['CUDA_VISIBLE_DEVICES'] = args.cuda_visible_devices

dataset_name = args.dataset
train_txt_name = 'train.txt'
val_txt_name = 'val.txt'
save_pth_name = '/data/ztl/CCAI/result_modules/module_cat_yc/' + dataset_name + '/'

def setup_seed(seed):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)  # 为了禁止hash随机化，使得实验可复现
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)  # if you are using multi-GPU.
    torch.backends.cudnn.benchmark = False
    # torch.backends.cudnn.enabled = False
    torch.backends.cudnn.deterministic = True

def _init_fn(worker_id):
    random.seed(10 + worker_id)
    np.random.seed(10 + worker_id)
    torch.manual_seed(10 + worker_id)
    torch.cuda.manual_seed(10 + worker_id)
    torch.cuda.manual_seed_all(10 + worker_id)

def init(module):
    if isinstance(module, nn.Conv3d) or isinstance(module, nn.ConvTranspose3d):
        nn.init.kaiming_normal_(module.weight.data, 0.25)
        nn.init.constant_(module.bias.data, 0)

def partial_load_model(model, model_path, device):
    """
    Load weight from model_path to model.
    Only weights with the same key name will be loaded, other weights will
    be ignored.
    """
    model.eval()
    print("loading ", type(model).__name__, " from ", model_path)
    saved_state_dict = torch.load(model_path, map_location=device)

    model_state_dict = model.state_dict()
    # filter state dict
    filtered_dict = {k: v for k, v in saved_state_dict.items() if k in model_state_dict}
    if len(filtered_dict) == len(saved_state_dict):
        print("model fully fits saved weights, performing complete load")
    else:
        print("model and saved weights doesn't fully match, performing partial load. common states: ",
              len(filtered_dict), ", saved states: ", len(saved_state_dict))
        print("an item in saved dict is: ")
        for key, _ in saved_state_dict.items():
            print(key)
            break
    model_state_dict.update(filtered_dict)
    model.load_state_dict(model_state_dict)
    return model

def pretrain(net, train_dl, device, para, args):
    '''
    Train the model to reconstruct input image
    In this case, the model has the same output shape as input. Therefore, it
    can be directly used for reconstruction without modification.
    '''
    assert args.pretrain_weight is not None, 'Path to save weight (pretrain-weight) must be specified'
    loss_func = nn.L1Loss()
    optimizer = torch.optim.Adam(net.parameters(), lr=para.learning_rate)
    net.train()
    for epoch in tqdm(args.pretrain_epochs):
        for step, (ct, _) in enumerate(train_dl):
            ct = ct.to(device)
            reconstruction = net(ct)
            loss = loss_func(reconstruction, ct)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            if step % 10 == 0:
                print(f"Epoch {epoch} step {step}: reconstruction loss: {loss.item() / ct.shape[0]}") # ct.size(0) is batch size
    # The given path must point to a file instead of a directory
    Path(args.pretrain_weight).parent.mkdir(parents=True, exist_ok=True)
    # Save pretrained weight
    torch.save(net.state_dict(), args.pretrain_weight)


def train(net, train_dl, val_dl, device, para, args):

    loss_func_list = [DiceLoss(), ELDiceLoss(), WCELoss(), JaccardLoss(), SSLoss(), TverskyLoss(), HybridLoss(), BCELoss()]
    loss_func = loss_func_list[0]
    # dice = Dice()

    optimizer = torch.optim.Adam(net.parameters(), lr=para.learning_rate)
    lr_decay = torch.optim.lr_scheduler.MultiStepLR(optimizer, para.learning_rate_decay)

    if args.pretrain_weight is not None:
        partial_load_model(net, args.pretrain_weight, device)

    # Training iteration
    start = time()
    for epoch in tqdm(para.Epoch):
        net.train()
        for step, (ct, seg) in enumerate(train_dl):

            ct = ct.to(device)
            seg = seg.to(device)
            outputs = net(ct)
            loss = loss_func(outputs, seg)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if step % 5 == 0:
                print('epoch:{}, step:{}, loss:{:.3f},time:{:.3f} min'.format(epoch, step, loss.item(), (time() - start) / 60))
        lr_decay.step()
    torch.save(net.state_dict(), save_pth_name + 'net.pth')


if __name__ == '__main__':

    model_config = {
        'in_chns': 1,
        'feature_chns': [16, 32, 64, 128, 256],
        'dropout': [0, 0, 0.3, 0.4, 0.5],
        'class_num': 1,
        'bilinear': True,
        'input_resolution': [112, 112],
        'num_transformer_blocks': [2, 2, 2, 2],
        'num_heads': [1, 4, 4, 8, 16], #1231
        'window_size': 7,
        'qkv_bias': True,
        'qk_scale': None,
        'drop_rate': 0.,
        'attn_drop_rate': 0.,
        'drop_path_rate': 0.1,
        'mlp_ratio': 4.,
        'use_checkpoint': False
    }

    # 使实验能够复现
    setup_seed(1092)

    net = CATNet(model_config)
    net.apply(init)

    # 计算网络参数
    print('net total parameters:', sum(param.numel() for param in net.parameters()))

    # 定义网络
    net = torch.nn.DataParallel(net)
    device = torch.device('cuda')
    net.to(device)

    # 定义Dateset
    train_ds = MyDataset(
        os.path.join(para.txt_pth+dataset_name, train_txt_name), 
        dataset_name, 
        transform = transforms.Compose([
                        RandomRotFlip(),
                        RandomCrop((112, 112, 80)),
                        ToTensor(),
                    ])
    )
    val_ds = MyDataset(os.path.join(para.txt_pth+dataset_name, val_txt_name), 
        dataset_name, 
        transform = transforms.Compose([
                        CenterCrop((112, 112, 80)),
                        ToTensor()
                    ])
    )

    # 定义数据加载
    train_dl = DataLoader(train_ds, para.batch_size, num_workers=para.num_workers, pin_memory=para.pin_memory, worker_init_fn=_init_fn)
    val_dl = DataLoader(val_ds, para.batch_size, num_workers=para.num_workers, pin_memory=para.pin_memory, worker_init_fn=_init_fn)

    if args.task == 'pretrain':
        pretrain(net, train_dl, device, para, args)

    if args.task == 'train':
        train(net, train_dl, val_dl, device, para, args)

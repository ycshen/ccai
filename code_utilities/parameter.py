# -----------------------路径相关参数---------------------------------------

odata_ct_path = '/data/ztl/CCAI/dataset/odata/'  # 原始训练集CT数据路径

odata_seg_path = '/data/ztl/CCAI/dataset/odata/'  # 原始训练集标注数据路径

pdata_set_path = '/data/ztl/CCAI/dataset/pdata/'  # 处理后的数据

txt_pth = '/data/ztl/CCAI/dataset/txt_pth/'  # 用来训练网络的数据保存地址

pred_path_res = '/data/ztl/CCAI/result_test-ouputs/prediction_resu/'  # 网络预测结果保存路径
pred_path_ulight = '/data/ztl/CCAI/result_test-ouputs/prediction_ulight/'  # 网络预测结果保存路径
pred_path_unr = '/data/ztl/CCAI/result_test-ouputs/prediction_unr/'
pred_path_u25 = '/data/ztl/CCAI/result_test-ouputs/prediction_u25/'
pred_path_v = '/data/ztl/CCAI/result_test-ouputs/prediction_v/'
pred_path_scse = '/data/ztl/CCAI/result_test-ouputs/prediction_scse/'
pred_path_unr_v2 = '/data/ztl/CCAI/result_test-ouputs/prediction_unr_v2/'
pred_path_unr_v3 = '/data/ztl/CCAI/result_test-ouputs/prediction_unr_v3/'
pred_path_ffc = '/data/ztl/CCAI/result_test-ouputs/prediction_ffc/'
pred_path_u2d = '/data/ztl/CCAI/result_test-ouputs/prediction_u2d/'
pred_path_attention = '/data/ztl/CCAI/result_test-ouputs/prediction_attention/'
pred_path_cople = '/data/ztl/CCAI/result_test-ouputs/prediction_cople/'
pred_path_nest = '/data/ztl/CCAI/result_test-ouputs/prediction_nest/'
pred_path_scse2d = '/data/ztl/CCAI/result_test-ouputs/prediction_scse2d/'
pred_path_ffc_1 = '/data/ztl/CCAI/result_test-ouputs/prediction_ffc_1/'
pred_path_u2d_1 = '/data/ztl/CCAI/result_test-ouputs/prediction_u2d_1/'
pred_path_attention_1 = '/data/ztl/CCAI/result_test-ouputs/prediction_attention_1/'
pred_path_cople_1 = '/data/ztl/CCAI/result_test-ouputs/prediction_cople_1/'
pred_path_nest_1 = '/data/ztl/CCAI/result_test-ouputs/prediction_nest_1/'
pred_path_scse2d_1 = '/data/ztl/CCAI/result_test-ouputs/prediction_scse2d_1/'
pred_path_ffc_2 = '/data/ztl/CCAI/result_test-ouputs/prediction_ffc_2/'
pred_path_u2d_2 = '/data/ztl/CCAI/result_test-ouputs/prediction_u2d_2/'
pred_path_attention_2 = '/data/ztl/CCAI/result_test-ouputs/prediction_attention_2/'
pred_path_cople_2 = '/data/ztl/CCAI/result_test-ouputs/prediction_cople_2/'
pred_path_nest_2 = '/data/ztl/CCAI/result_test-ouputs/prediction_nest_2/'
pred_path_scse2d_2 = '/data/ztl/CCAI/result_test-ouputs/prediction_scse2d_2/'
pred_path_nest_3 = '/data/ztl/CCAI/result_test-ouputs/prediction_nest_3/'
pred_path_ffc_02 = '/data/ztl/CCAI/result_test-ouputs/prediction_ffc_02/'
pred_path_ffc_12 = '/data/ztl/CCAI/result_test-ouputs/prediction_ffc_12/'
pred_path_ffc_22 = '/data/ztl/CCAI/result_test-ouputs/prediction_ffc_22/'
pred_path_nest_4 = '/data/ztl/CCAI/result_test-ouputs/prediction_nest_4/'
pred_path_nest_5 = '/data/ztl/CCAI/result_test-ouputs/prediction_nest_5/'
pred_path_stacking_3 = '/data/ztl/CCAI/result_test-ouputs/prediction_stacking_3/'
pred_path_stacking_4 = '/data/ztl/CCAI/result_test-ouputs/prediction_stacking_4/'
pred_path_stacking_5 = '/data/ztl/CCAI/result_test-ouputs/prediction_stacking_5/'
pred_path_nest_6 = '/data/ztl/CCAI/result_test-ouputs/prediction_nest_6/'
pred_path_nest_7 = '/data/ztl/CCAI/result_test-ouputs/prediction_nest_7/'
pred_path_nest_8 = '/data/ztl/CCAI/result_test-ouputs/prediction_nest_8/'
pred_path_cat_1 = '/data/ztl/CCAI/result_test-ouputs/prediction_cat_1/'
pred_path_cat_2 = '/data/ztl/CCAI/result_test-ouputs/prediction_cat_2/'
pred_path_cat_3 = '/data/ztl/CCAI/result_test-ouputs/prediction_cat_3/'
pred_path_swin = '/data/ztl/CCAI/result_test-ouputs/prediction_swin/'
pred_path_transunet = '/data/ztl/CCAI/result_test-ouputs/prediction_transunet/'
pred_path_utnet = '/data/ztl/CCAI/result_test-ouputs/prediction_utnet/'
pred_path_mtnet = '/data/ztl/CCAI/result_test-ouputs/prediction_mtnet/'



module_path_res = '/data/ztl/CCAI/result_modules/module_resu/'  # 测试模型地址
module_path_ulight = '/data/ztl/CCAI/result_modules/module_ulight/'  # 测试模型地址
module_path_unr = '/data/ztl/CCAI/result_modules/module_unr/'
module_path_u25 = '/data/ztl/CCAI/result_modules/module_u25/'
module_path_v = '/data/ztl/CCAI/result_modules/module_v/'
module_path_scse = '/data/ztl/CCAI/result_modules/module_scse/'
module_path_unr_v2 = '/data/ztl/CCAI/result_modules/module_unr_v2/'
module_path_unr_v3 = '/data/ztl/CCAI/result_modules/module_unr_v3/'
module_path_ffc = '/data/ztl/CCAI/result_modules/module_ffc/'
module_path_u2d = '/data/ztl/CCAI/result_modules/module_u2d/'
module_path_attention = '/data/ztl/CCAI/result_modules/module_attention/'
module_path_cople = '/data/ztl/CCAI/result_modules/module_cople/'
module_path_nest = '/data/ztl/CCAI/result_modules/module_nest/'
module_path_scse2d = '/data/ztl/CCAI/result_modules/module_scse2d/'
module_path_ffc_1 = '/data/ztl/CCAI/result_modules/module_ffc_1/'
module_path_u2d_1 = '/data/ztl/CCAI/result_modules/module_u2d_1/'
module_path_attention_1 = '/data/ztl/CCAI/result_modules/module_attention_1/'
module_path_cople_1 = '/data/ztl/CCAI/result_modules/module_cople_1/'
module_path_nest_1 = '/data/ztl/CCAI/result_modules/module_nest_1/'
module_path_scse2d_1 = '/data/ztl/CCAI/result_modules/module_scse2d_1/'
module_path_ffc_2 = '/data/ztl/CCAI/result_modules/module_ffc_2/'
module_path_u2d_2 = '/data/ztl/CCAI/result_modules/module_u2d_2/'
module_path_attention_2 = '/data/ztl/CCAI/result_modules/module_attention_2/'
module_path_cople_2 = '/data/ztl/CCAI/result_modules/module_cople_2/'
module_path_nest_2 = '/data/ztl/CCAI/result_modules/module_nest_2/'
module_path_scse2d_2 = '/data/ztl/CCAI/result_modules/module_scse2d_2/'
module_path_nest_3 = '/data/ztl/CCAI/result_modules/module_nest_3/'
module_path_ffc_02 = '/data/ztl/CCAI/result_modules/module_ffc_02/'
module_path_ffc_12 = '/data/ztl/CCAI/result_modules/module_ffc_12/'
module_path_ffc_22 = '/data/ztl/CCAI/result_modules/module_ffc_22/'
module_path_nest_4 = '/data/ztl/CCAI/result_modules/module_nest_4/'
module_path_nest_5 = '/data/ztl/CCAI/result_modules/module_nest_5/'
module_path_stacking_3 = '/data/ztl/CCAI/result_modules/module_stacking_3/'
module_path_stacking_4 = '/data/ztl/CCAI/result_modules/module_stacking_4/'
module_path_stacking_5 = '/data/ztl/CCAI/result_modules/module_stacking_5/'
module_path_nest_6 = '/data/ztl/CCAI/result_modules/module_nest_6/'
module_path_nest_7 = '/data/ztl/CCAI/result_modules/module_nest_7/'
module_path_nest_8 = '/data/ztl/CCAI/result_modules/module_nest_8/'
module_path_cat_1 = '/data/ztl/CCAI/result_modules/module_cat_1/'
module_path_cat_2 = '/data/ztl/CCAI/result_modules/module_cat_2/'
module_path_cat_3 = '/data/ztl/CCAI/result_modules/module_cat_3/'
module_path_ccat = '/data/ztl/CCAI/result_modules/module_ccat/'
module_path_swin = '/data/ztl/CCAI/result_modules/module_swin/'
module_path_transunet = '/data/ztl/CCAI/result_modules/module_transunet/'
module_path_utnet = '/data/ztl/CCAI/result_modules/module_utnet/'
module_path_mtnet = '/data/ztl/CCAI/result_modules/module_mtnet/'



# -----------------------路径相关参数---------------    ------------------------


# ---------------------训练数据获取相关参数-----------------------------------

size = 48  # 使用48张连续切片作为网络的输入

down_scale = 0.5  # 横断面降采样因子

expand_slice = 20  # 仅使用包含肝脏以及肝脏上下20张切片作为训练样本

slice_thickness = 1  # 将所有数据在z轴的spacing归一化到1mm

upper, lower = 200, -200  # CT数据灰度截断窗口

# ---------------------训练数据获取相关参数-----------------------------------


# -----------------------网络结构相关参数------------------------------------

drop_rate = 0.3  # dropout随机丢弃概率

# -----------------------网络结构相关参数------------------------------------


# ---------------------网络训练相关参数--------------------------------------

# gpu = '1'  # 使用的显卡序号

Epoch = 500

learning_rate = 1e-4

learning_rate_decay = [500, 750]

alpha = 0.33  # 深度监督衰减系数

batch_size = 1

num_workers = 3

pin_memory = True

cudnn_benchmark = True

# ---------------------网络训练相关参数--------------------------------------


# ----------------------模型测试相关参数-------------------------------------

threshold = 0.5  # 阈值度阈值

stride = 12  # 滑动取样步长

maximum_hole = 5e4  # 最大的空洞面积

# ----------------------模型测试相关参数-------------------------------------


# ---------------------CRF后处理优化相关参数----------------------------------

# z_expand, x_expand, y_expand = 10, 30, 30  # 根据预测结果在三个方向上的扩展数量
#
# max_iter = 20  # CRF迭代次数
#
# s1, s2, s3 = 1, 10, 10  # CRF高斯核参数

# ---------------------CRF后处理优化相关参数----------------------------------
import os
import math
import random

path = '/data/ztl/CCAI/dataset/pdata/tumor/ct/'
files = os.listdir(path)

train_txt = open('/data/ztl/CCAI/dataset/txt_pth/tumor/train.txt', 'a')
val_txt = open('/data/ztl/CCAI/dataset/txt_pth/tumor/val.txt', 'a')

for file in files:
    tmp_name = file.split('.')[0]
    tmp_name = tmp_name.split('-')[-1]
    if int(tmp_name) in range(27, 49):
        words = path + file + ' ' + path.replace('ct', 'seg') + file.replace('volume', 'segmentation') + '.gz' + '\n'
        val_txt.write(words)
    else:
        words = path + file + ' ' + path.replace('ct', 'seg') + file.replace('volume', 'segmentation') + '.gz' + '\n'
        train_txt.write(words)

# data_pth = '/data/ztl/CCAI/dataset/pdata/heart/'
#
# train_txt = open('/data/ztl/CCAI/dataset/txt_pth/heart/train.txt', 'a')
# val_txt = open('/data/ztl/CCAI/dataset/txt_pth/heart/val.txt', 'a')
# with open('/data/ztl/CCAI/dataset/odata/heart/train.list', 'r') as f:
#     data_list = f.readlines()
# data_list = [item.replace('\n', '') for item in data_list]
# for file in data_list:
#     word = data_pth + file + '/mri_norm2.h5' + '\n'
#     train_txt.write(word)
#
# with open('/data/ztl/CCAI/dataset/odata/heart/test.list', 'r') as f:
#     data_list = f.readlines()
# data_list = [item.replace('\n', '') for item in data_list]
# for file in data_list:
#     word = data_pth + file + '/mri_norm2.h5' + '\n'
#     val_txt.write(word)







# path = '/data/ztl/CCAI/pdata/ct/'
# files = os.listdir(path)
# val_len = math.ceil(len(files)*0.2)
# random.shuffle(files)
#
# train_txt_1 = open('/data/ztl/CCAI/txt_pth/train_1.txt', 'a')
# val_txt_1 = open('/data/ztl/CCAI/txt_pth/val_1.txt', 'a')
# train_list_1 = files[val_len:]
# val_list_1 = files[:val_len]
# print('train_1 length :', len(train_list_1))
# print('val_1_length :', len(val_list_1))
# for file in train_list_1:
#     words = path + file + ' ' + path.replace('ct', 'seg') + file.replace('volume', 'segmentation') + '.gz' + '\n'
#     train_txt_1.write(words)
# for file in val_list_1:
#     words = path + file + ' ' + path.replace('ct', 'seg') + file.replace('volume', 'segmentation') + '.gz' + '\n'
#     val_txt_1.write(words)
#
# train_txt_2 = open('/data/ztl/CCAI/txt_pth/train_2.txt', 'a')
# val_txt_2 = open('/data/ztl/CCAI/txt_pth/val_2.txt', 'a')
# train_list_2 = files[:val_len] + files[val_len*2:]
# val_list_2 = files[val_len:val_len*2]
# print('train_2 length :', len(train_list_2))
# print('val_2_length :', len(val_list_2))
# for file in train_list_2:
#     words = path + file + ' ' + path.replace('ct', 'seg') + file.replace('volume', 'segmentation') + '.gz' + '\n'
#     train_txt_2.write(words)
# for file in val_list_2:
#     words = path + file + ' ' + path.replace('ct', 'seg') + file.replace('volume', 'segmentation') + '.gz' + '\n'
#     val_txt_2.write(words)
#
# train_txt_3 = open('/data/ztl/CCAI/txt_pth/train_3.txt', 'a')
# val_txt_3 = open('/data/ztl/CCAI/txt_pth/val_3.txt', 'a')
# train_list_3 = files[:val_len*2] + files[val_len*3:]
# val_list_3 = files[val_len*2:val_len*3]
# print('train_3 length :', len(train_list_3))
# print('val_3_length :', len(val_list_3))
# for file in train_list_3:
#     words = path + file + ' ' + path.replace('ct', 'seg') + file.replace('volume', 'segmentation') + '.gz' + '\n'
#     train_txt_3.write(words)
# for file in val_list_3:
#     words = path + file + ' ' + path.replace('ct', 'seg') + file.replace('volume', 'segmentation') + '.gz' + '\n'
#     val_txt_3.write(words)
#
# train_txt_4 = open('/data/ztl/CCAI/txt_pth/train_4.txt', 'a')
# val_txt_4 = open('/data/ztl/CCAI/txt_pth/val_4.txt', 'a')
# train_list_4 = files[:val_len*3] + files[val_len*4:]
# val_list_4 = files[val_len*3:val_len*4]
# print('train_4 length :', len(train_list_4))
# print('val_4_length :', len(val_list_4))
# for file in train_list_4:
#     words = path + file + ' ' + path.replace('ct', 'seg') + file.replace('volume', 'segmentation') + '.gz' + '\n'
#     train_txt_4.write(words)
# for file in val_list_4:
#     words = path + file + ' ' + path.replace('ct', 'seg') + file.replace('volume', 'segmentation') + '.gz' + '\n'
#     val_txt_4.write(words)
#
# train_txt_5 = open('/data/ztl/CCAI/txt_pth/train_5.txt', 'a')
# val_txt_5 = open('/data/ztl/CCAI/txt_pth/val_5.txt', 'a')
# train_list_5 = files[:val_len*4]
# val_list_5 = files[val_len*4:]
# print('train_5 length :', len(train_list_5))
# print('val_5_length :', len(val_list_5))
# for file in train_list_5:
#     words = path + file + ' ' + path.replace('ct', 'seg') + file.replace('volume', 'segmentation') + '.gz' + '\n'
#     train_txt_5.write(words)
# for file in val_list_5:
#     words = path + file + ' ' + path.replace('ct', 'seg') + file.replace('volume', 'segmentation') + '.gz' + '\n'
#     val_txt_5.write(words)








